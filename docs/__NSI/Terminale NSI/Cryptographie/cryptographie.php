<!DOCTYPE html>
<html>
  <?php include("../../../templates/header.php"); ?>

    <body class="has-navbar-fixed-top">
      
        <?php include("../../../templates/navbar.php"); ?>
    
        <!--               SECTION ET CONTAINER                                     -->
        <section class="hero is-black is-bold">
            <div class="hero-body">
            <div class="container">
                <h2 class="title">
                Terminale NSI - Architecture - Système d'exploitation - Sécurisation des informations
                </h2>
            </div>
            </div>
        </section>

        <br/>
        <section>
            <div class="container content">
                <div class="message">
                    <div class="message-body">
                    <h6><a href="http://revue.sesamath.net/spip.php?article403" target="blank">Article de la revue Sesamath dont sont tirés éléments' ci-dessous :</a></h6>
                    <table>
                    <tr>
                        <td><?php include("Cesar.html"); ?></td>
                        <td><?php include("Vigenere.html"); ?></td>
                        <td><?php include("Affine.html"); ?></td>
                    </tr>
                    <tr>
                        <td><?php include("Hill.html"); ?></td>
                        <td><?php include("scytale.html"); ?></td>
                        <td><?php include("RSA.html"); ?></td>
                    </tr>
                    </table>
                    </div>
            </div>
        </section>

        

    </body>
</html>
