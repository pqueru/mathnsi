# BAC

## Définition de l'épreuve 

[ Note de service n° 2020-030 du 11-2-2020](https://www.education.gouv.fr/bo/20/Special2/MENE2001797N.htm)

### Partie écrite

Durée : 3 heures 30

[Sujets officiels 2022](https://www.education.gouv.fr/reussir-au-lycee/bac-2022-les-sujets-des-epreuves-ecrites-de-specialite-341303) ; toutes matières.

### Partie pratique

Durée : 1 heure

La [Banque Nationale de Sujets](https://eduscol.education.fr/2661/banque-des-epreuves-pratiques-de-specialite-nsi) donne les 40 sujets possibles pour le BAC 2022.

On peut aussi s'entrainer avec les sections `À maitriser` et `Guidés` de <https://e-nsi.gitlab.io/pratique/>

## Adaptation

Une partie du programme de NSI ne figurera pas à l'épreuve du baccalauréat. On pourra donc la traiter après les épreuves.

[Note de service du 12-7-2021](https://www.education.gouv.fr/bo/21/Hebdo30/MENE2121274N.htm)

!!! warning "Hors programme BAC"

    À compter de la session 2022 du baccalauréat, les parties du programme de terminale qui ne pourront pas faire l'objet d'une évaluation lors de l'épreuve terminale écrite et pratique de l'enseignement de spécialité numérique et sciences informatiques de la classe de terminale de la voie générale définie dans la note de service n° 2020-030 du 11 février 2020 sont définies comme suit :

    1. Histoire de l'informatique
      
        - Évènements clés de l'histoire de l'informatique
    2. Structures de données
      
        - Graphes : structures relationnelles. Sommets, arcs, arêtes, graphes orientés ou non orientés
    3. Bases de données
      
        - Système de gestion de bases de données relationnelles
    4. Architectures matérielles, systèmes d'exploitation et réseaux
      
        - Sécurisation des communications
    5. Langages et programmation
      
        - Notions de programme en tant que donnée. Calculabilité, décidabilité
        - Paradigmes de programmation
    6. Algorithmique
      
        - Algorithmes sur les graphes
        - Programmation dynamique
        - Recherche textuelle


## Un exemple d'exercice de l'épreuve écrite

> D'après 2022, Centres étrangers, J2, Ex. 1

**1.** Voici une fonction codée en Python :

```python
def f(n):
    if n == 0:
        print("Partez !")
    else:
        print(n)
        f(n-1)
```

**1.a.** Qu'affiche la commande `f(5)` ?

??? done "Réponse"
    `f(5)` affiche

    ```
    5
    4
    3
    2
    1
    Partez !
    ```

**1.b.** Pourquoi dit-on de cette fonction qu'elle est récursive ?

??? done "Réponse"
    Le code source de `f` contient un appel à elle-même, c'est donc une fonction récursive.




**2.** On rappelle qu'en python l'opérateur `+` a le comportement suivant sur les chaines de caractères :

```pycon
>>> S = 'a' + 'bc'
>>> S
'abc'
```

Et le comportement suivant sur les listes :

```pycon
>>> L = ['a'] + ['b', 'c']
>>> L
['a', 'b', 'c']
```

On a besoin pour les questions suivantes de pouvoir ajouter une chaine de caractères `s` en préfixe à chaque chaine de caractères de la liste `chaines`.

On appellera cette fonction `ajouter`.

Par exemple, `#!py ajouter("a", ["b", "c"])` doit renvoyer `["ab", "ac"]`.

**2.a.** Recopiez le code suivant et complétez `...` sur votre copie :

```python
def ajouter(s, chaines):
    resultat = []
    for mot in chaines:
        resultat ...
    return resultat
```

??? done "Réponses"

    ```python
    def ajouter(s, chaines):
        resultat = []
        for mot in chaines:
            resultat.append(s + mot)
        return resultat
    ```

**2.b.** Que renvoie la commande `#!py ajouter("b", ["a", "b", "c"])` ?

??? done "Réponse"
    `#!py ajouter("b", ["a", "b", "c"])` renvoie `["ba", "bb", "bc"]`.

**2.c.** Que renvoie la commande `#!py ajouter("a", [""])` ?

??? done "Réponse"
    `#!py ajouter("a", [""])` renvoie `["a"]`.

**3.** On s'intéresse ici à la fonction suivante écrite en Python où `s` est une chaine de caractères et `n` un entier naturel.

```python
def produit(s, n):
    if n == 0:
        return [""]
    else:
        resultat = []
        for c in s:
            resultat = resultat + ajouter(c, produit(s, n - 1))
        return resultat
```

**3.a.** Que renvoie la commande `#!py produit("ab", 0)` ? Le résultat est-il une liste vide ?

??? done "Réponse"
    `#!py produit("ab", 0)` utilise le paramètre `n` égal à `0`, donc elle renvoie `#!py [""]`. (L'affichage en console sera `#!py ['']`)

    `#!py [""]` n'est pas une liste vide, c'est une liste qui contient **un** élément : la chaine de caractères vide.

**3.b.** Que renvoie la commande `#!py produit("ab", 1)` ?

??? done "Réponse"
    `#!py produit("ab", 1)` fait une boucle `#!py for` avec deux tours :

    - Premier tour, avec `c = 'a'`, `resultat` devient `["a"]`.
    - Second tour, avec `c = 'b'`,  `resultat` devient `["a"] + ["b"]`.

    `#!py ['a', 'b']` est renvoyé.

**3.c.** Que renvoie la commande `#!py produit("ab", 2)` ?

??? done "Réponse"
    `#!py produit("ab", 2)` fait une boucle `#!py for` avec deux tours :

    - Premier tour, avec `c = 'a'`, `resultat` devient `["aa", "ab"]`.
    - Second tour, avec `c = 'b'`,  `resultat` devient `["aa", "ab"] + ["ba", "bb"]`.

    `#!py ['aa', 'ab', 'ba', 'bb']` est renvoyé.

