# ⚠️ Gestion des erreurs

## Erreurs classiques

### Typage dynamique

Python est un langage à typage dynamique: il n'impose pas que le type des variables soit défini. Le type est alors déterminé à la volée au moment de l'interprétation du code. On peut donc à loisir changer le type de données d'une variable. Cela peut poser problème si on n'est pas prudent. 

D'autres langages, tels le C, Java ou Pascal sont à typage statique, donc nettement moins permissifs que Python. Il est alors nécessaire de déclarer le type de chaque variable au début du programme.

Testez et analysez ce programme :

```python
a = "Hello World"
print(type(a))
a = 12
print(type(a))
```

{{terminal()}}

### Effet de bord

On appelle effet de bord la modification de valeurs référencées par des variables. Cela peut poser problème dans le cas de variables mutables, notamment lors de la copie d'une variable par affectation. En effet, lors d'une affectation du type `b = a`, ce n'est pas la valeur de $a$ qui est recopiée, mais son **identifiant**!

!!! note "Cas des variables **non mutables**"
    === "Cas des variables **non mutables**"

        ```python
        a = 1961
        b = a
        print(a, "\t", b)

        a = 1935
        print(a, "\t", b)
        ```

        {{terminal()}}

        Comme les identifiants de `a` et `b` sont identiques, ils définissent la même plage mémoire pour les valeurs de ces deux variables. Autrement dit `a` et `b` ont la même valeur.

        Cependant, toute modification de la variable `a` via une affectation créera une autre plage mémoire avec un nouvel identifiant : la variable `b` ne sera donc pas modifiée !

    === "Cas des variables **mutables**" 
        Les variables mutables peuvent être modifiées par d'autres opérations qu'une affectation. Or cela conserve l'identifiant ! Par conséquent, pour une variable de type mutable, l'identifiant ainsi que la plage mémoire allouée à la valeur de cette variable peuvent rester inchangés, même lorsque l'on change la valeur de cette variable.

        ```python
        L1 = [1, 9, 6, 1]
        L2 = L1
        print(L1, "\t", L2)

        L1[2: 4] = [3, 5]
        print(L1, "\t", L2)
        ```

        {{terminal()}}

        Cette manière de procéder permet un gain de temps (copie rapide) et de mémoire (une seule plage mémoire pour deux variables). Mais elle pose un problème technique : si le contenu de la plage mémoire de la valeur de `L1` est modifié, cela entraine automatiquement la modification de la valeur de `L2` puisqu'ils sont référencés par le même identifiant!

        Pour le programmeur Python non averti, cet **effet de bord** peut provoquer une erreur de programmation difficile à trouver.

        On peut néanmoins copier une liste sans provoquer d'effet de bord en créant une nouvelle liste et en copiant les valeurs de la première liste une à une dans cette nouvelle liste ou, plus simplement en utilisant la fonction `deepcopy` de la biliothèque `copy` :

        ```python
        from copy import deepcopy

        L1 = [1, 9, 6, 1]
        L2 = deepcopy(L1)
        print(L1, "\t", L2)

        L1[2: 4] = [3, 5]
        print(L1, "\t", L2)
        ```

        {{terminal()}}

Un programme écrit selon le **paradigme fonctionnel** utilise des fonctions dites fonctionnelles (!) et se caractérisent essentiellement par une chose : l'absence d'effets de bord. Le code ne dépend pas de données se trouvant à l'extérieur de la fonction courante et il ne modifie pas des données à l'extérieur de cette fonction. Il évite donc l'utilisation de variables globales.

Comparez les deux exemples ci-dessous :

```python
a = 0

def increment_non_fonctionnel():
  global a
  a = a + 1
  return a

print(increment_non_fonctionnel())
print(increment_non_fonctionnel())
```

{{IDE('scripts/increment_non_fonctionnel')}}

```python
a = 0

def increment_fonctionnel(a):
  return a + 1
  
print(increment_fonctionnel(a))
print(increment_fonctionnel(a))
```

{{IDE('scripts/increment_fonctionnel')}}

!!! info "Portée d'une variable"
    La **portée** d'une variable correspond aux parties du programme pour lesquelles la variable est définie.

    - Une variable définie à l'intérieur d'une fonction est une variable **locale**. Elle ne sera pas reconnue au sein d'une autre fonction ni au sein du programme principal.

    - Une variable définie au niveau du programme principal est une variable **globale**. Elle est reconnue partour dans le programme, même au sein des fonctions.

    On peut faire en sorte qu'une variable déclarée au sein d'une fonction soit malgré tout une variable globale en utilisant l'instruction **global**. Mais c'est à éviter pour cause de confusion.

!!! info "Résumé"
    On appelle effet de bord la modification de valeurs référencées par des variables. Pour les éviter, le code intérieur d'une fonction ne doit pas modifier le code extérieur.

    La copie de variables non mutables ne pose pas de souci. Mais la copie de variables mutables (listes, dictionnaires...) provoque un effet de bord. Il faut alors utiliser une copie "profonde".

### Débordement

Il existe deux sortes de débordement:

On peut constater un débordement négatif sur une liste si on appelle la méthode `.pop()` sans s'assurer au préalable que la liste contient au moins un élément. On sera confronté à un débordement positif lorsqu'on parcourt une liste au-delà de son dernier élément.

### Les exceptions

Même si un code est syntaxiquement correct, il peut générer des erreurs. Quand Python rencontre une erreur lors de l'exécution d'un code, on dit qu'il **lève une exception**.

Cela génère l'affichage d'un message explicite et l'arrêt du programme si ces exceptions ne sont pas gérées.

**Gérer les exceptions** consiste à **anticiper les erreurs** (hors de contrôle du concepteur) de manière à ce que le programme ne s'arrête pas. Contrairement aux assertions, les exceptions permettent ainsi de ne pas faire planter le programme en proposant une alternative de traitement des cas problématiques avec les instructions `try/except/else/finally`. Elles doivent **demeurer dans le code final**.

En effet, la plupart des programmes ont vocation à être utilisés par d'autres utilisateurs que leur concepteur, et ceux-ci peuvent entrer des valeurs de variables provoquant des erreurs lors de l'exécution, même si le code du concepteur est initialement correct. Le concepteur doit anticiper ces erreurs dues à l'utilisateur.

#### Liste non exhaustive d'exceptions

Il existe un certain nombre de catégories d'exceptions clairement identifiées et repérées par un nom adéquat.

**Exercice 8**

Dans les lignes ci-dessous, un certain nombre de codes erronés sont écrits. Identifier l'exception corresponde parmi celles proposées ci-dessous et associées les au code.

```
a) SyntaxError: invalid syntax
b) ZeroDivisionError: division by zero
c) FileNotFoundError: [Errno 2] No such file or directory: 'mon_fichier'
d) IndexError: list index out of range
e) ValueError: invalid literal for int() with base 10: 'seize'
f) TypeError: unsupported operand type(s) for +: 'int' and 'str'
g) NameError: name 'z' is not defined
```

Cette liste est non exhaustive...

1. 
```python
somme = 0
L = [2, 5, '6', 9]

for element in L:
    somme = somme + element
```
2. 
```python
def div(x, y):
    return x / y

for i in range(5):
    print(div(5, i))
```
3. 
```python
x = 2
y = x + z
print(y)
```
4. 
```python
l = [1, 2, 3]

for i in range(5):
    print(l[i])
```
5. 
```python
age = "seize"
print(int(age))
```
6. 
```python
for i in range(5)
    print(i)
```
7. 
```python
open('mon_fichier')
```

Au-dessus du type d'exception (ValueError, IndexError, etc...) se trouve une description de ce qui a causé l'erreur. C'est ce qu'on appelle une stack trace, et ça représente la pile d'appels qui ont amené à cette erreur. Pour comprendre votre erreur, il faut lire ce texte à l'envers, de bas en haut.

Testez et analysez le programme suivant :

```python
def une_fonction():
    return 1/0
  

def une_autre_fonction():
    une_fonction()


une_autre_fonction()
```

{{IDE('scripts/vide')}}

#### Comment gérer les exceptions?

Il suffit pour cela d'utiliser les instructions `try/except` et éventuellement `else` et `finally`.

- **Le bloc `try`** permet de tester un bloc de code et ne l'exécute que s'il ne contient aucune erreur. Dans le cas contraire, le programme ignore la totalité du code dans ce bloc et passe au bloc suivant `except`

- **Le bloc `except`** sera exécuté en cas d'erreur

- **Le bloc `else`** permet d'exécuter une acion si aucune erreur ne survient dans le bloc `try`

- **Le bloc `finally`** vous permet d'exécuter du code, quel que soit le résultat des blocs `try` et `except`.

*Exemple* : structure complète possible

```python
try:
  resultat = numerateur / denominateur

except NameError:
  print("La variable numerateur ou denominateur n'a pas été définie.")

except TypeError:
  print("La variable numerateur ou denomniateur possède un type incompatible avec la division")

except ZeroDivisionError:
  print("La variable denominateur est égale à 0.")

else:
  print("Le résultat est : ", resultat)

finally:
  print("Vous venez de calculer une division")
```

#### Lever une exception

Vous pouvez lever tout type d'exceptions avec l'instruction `raise`. Cela fonctionne un peu sur le même principe que les assertions.

Voici deux exemples :

```python
x = -1
if x < 0:
    raise Exception("pas de nombre négatif SVP")
```

{{IDE('scripts/vide')}}

```python
x = "Hello"
if type(x) != int:
    raise Exception("Seuls les entiers sont autorisés")
```

{{IDE('scripts/vide')}}