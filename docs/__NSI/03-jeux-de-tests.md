# 🚦 Jeux de tests

Afin de vérifier le bon fonctionnement d'un algorithme, il faut aussi tester son programme avec un jeu de tests judicieusement choisi.

Pour cela, on peut:

- Utiliser des assertions dans le programme principal

- Insérer des tests dans le prototype même de la fonction. Cela présente de gros avantages : la fonction peut être transférée à un autre programme, ou données à un autre programmeur : celui-ci aura tous les renseignements nécessaires.

Voici un exemple de test inséré dans le prototype d'une fonction :

```python
"""
...

Exemples :
>>> indice_maximum_liste([2, 0, 8, -3, 9, 6])
4
"""
```
Pour que ce test tapé dans le prototype de la fonction soit reconnu, exécuté et comparé au résultat souhaité, il faudra toutefois ajouter les trois lignes de code suivantes dans votre prorgamme principal :

```python
if __name__ == '__main__':
  import doctest
  
  doctest.testmod()
```

{{IDE('scripts/vide')}}