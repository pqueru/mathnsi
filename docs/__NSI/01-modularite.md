# 📚 Modularité

Un programme est découpé en modules logiciels afin de favoriser leur maintenance ou leur réutilisation dans d'autres contextes.

!!! info
    - Un **module** est un ensemble de fonctions prédéfinies
    - Une **bibliothèque** correspond à un ensemble plus complet puisqu'elle comprend des fonctions, des constantes ET des modules

Encore faut-il savoir utiliser le module désiré... D'où la nécessité de lire la documentation de ce module en utilisant l'instruction `help()` !

## Instructions `help()` et `dir()`

### Instruction `help()`

L'instruction `help(nom_de_la_fonction)` ou `help(module)` premet d'obtenir la liste des fonctions contenues dans ce module.

Testez dans la cellule ci-dessous, les lignes suivantes et analysez-les:

{{terminal()}}

```python
help(len)
```

```python
import math

help(math)
```

### Instruction `dir()`

Cette instruction permet de lister les attributs d'un objet (liste, tuple, dictionnaire, classe...).

```python
number1 = [2, 3, 1]

print(dir(number1))
```

{{terminal()}}

Ici, une longue liste d'attributs apparaît.

On s'aperçoit ainsi que l'on peut, entre autres :

- Ajouter un terme à la liste avec la méthode `append`
- Trier une liste avec la méthode `sort`

Vérifiez tout cela avec :

```python
number1.append(4)
print(number1)
number1.sort()
print(number1)
```

{{terminal()}}

!!! Résumé
    - L'instruction `help()` permet de se documenter sur des instructions, fonctions, modules...
    - L'instruction `dir()` permet de lister les attributs et méthodes d'un objet.


**Exercice**

Sur Thonny (ou tout autre logiciel de création de fichier `.py`) :

1. Créez un module nommé `algos_classiques_listes.py` dans lequel vous mettrez toutes les fonctions concernant les listes. A savoir :
    - Recherche de la présence d'un élément $e$ dans la liste $L$: `dans_liste(e, L)` 
    - Recherche de la valeur maximale (et/ou minimale) des élément d'une liste $L$: `max_liste(L)`
    - Calcul de la moyenne des élément d'une liste $L$: `moy_liste(L)`
    - Recherche de l'indice de l'élément minimum: `indice_min_liste(L)` 
    - Recherche du nombre d'occurrences d'une valeur: `card_val_liste(L)`
    - Suppression de la première occurrence d'un élément: `supp_liste(L)`
    - **Vous veillerez à respecter dans la mesure du possible les recommandations de PEP8.**
2. Sur JupyterLab, créez un Notebook nommé `Algorithmes classiques sur les listes`.
Attachez à ce notebook votre fichier `algos_classiques_listes.py`.
Dans des nouvelles cellules, copiez/coller le code ci-dessous et testez-le :
```python
import algos_classiques_listes as al

help(algos_classiques_listes)

help(dans_liste)

print(al.dans_liste(2,[1, 2, 3]))

print(al.max_liste([1, 2, 3]))

print(al.moy_liste([1, 2, 3]))

print(al.indice_min_liste([1, 2, 3]))

print(al.card_val_liste(2,[2, 2, 2]))

print(al.supp_liste(1,[2, 1, 4, 1, 3]))
```
