# 🚃 Tableau (liste en python)

!!! example "Exemple de tableau"

    |Indice|0|1|2|3|4|5|6|7|
    |:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
    |Adresse|15783|15791|15799|15807|15815|15823|15831|15839|
    |Valeur|65|14|69|25|78|22|10|17|

    On suppose ici que la taille de chaque case est 8 unités d'adresse.

    - L'adresse de départ du tableau est $15783$
    - L'adresse de l'élément d'indice 3 est $15783 + 3×8 = 15807$
    - Si on connait l'indice $i$, la taille $s$ des cases et l'adresse $a$ de départ,
        - alors l'adresse de l'élément d'indice $i$ est $a+i×s$
        - c'est facile à calculer, donc **l'accès est rapide**.


!!! info "Définition"
    Un tableau est une séquence ordonnée de valeur de même type, alignée dans la mémoire de l'ordinateur.

    C'est une structure simple en informatique.

    - Si on connait l'adresse du tableau, et la taille mémoire commune de chaque valeur, alors on peut trouver l'adresse de tout élément. On peut écrire ou lire une valeur.

    Il s'agit techniquement d'un objet en forme de ligne, mais on pourra créer des tableau de lignes, ce qui peut donner une image... Mais commençons par une seule dimension.

!!! warning "Liste ou Tableau ?"
    Pour débuter avec Python, on utilisera le type `list` qui peut se comporter comme si c'était un tableau.

    **On se limitera, au début, volontairement à quelques méthodes**.

    - Création d'un tableau d'une certaine longueur.
    - Lecture et écriture d'une valeur.

    Il existe d'autres méthodes, mais elle ne sont pas élémentaires.

## Création

### En dur

Pour créer un tableau dans un code Python, on utilise les délimiteurs :

- `[` : pour débuter
- `]` : pour finir
- `,` : pour séparer

On sépare les éléments par des virgules.

:warning: Même s'il est possible de construire des listes avec des éléments de types différents, nous le déconseillons. Nous parlerons de tableaux ici !

Exemples :

```python
jours_semaine = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
nb_premiers = [2, 3, 5, 7, 11]
hauteurs = [1.82, 1.75, 1.68, 1.89]
liste_vide = []
liste_avec_un_élément = [1337]
```

### Par concaténation

!!! danger "Pour une liste, pas un tableau"
    - On peut concaténer des listes avec l'opérateur `+`
    - On peut aussi répéter une concaténation avec l'opérateur `*`

    ```pycon
    >>> [0] + [0] + [0] + [0]
    [0, 0, 0, 0]
    >>> 7 * [0]
    [0, 0, 0, 0, 0, 0, 0]
    ```

:warning: Le programme officiel préconise plutôt une autre méthode, la construction des **listes en compréhension**.

On parlera désormais de listes, même si c'est d'un tableau qu'on parle.

### Les listes en compréhension

!!! info "Version simple"
    Avec un exemple

    ```pycon
    >>> [0 for _ in range(7)]
    [0, 0, 0, 0, 0, 0, 0]
    ```

    Oui, c'est plus long que `[0] * 7`, mais pas  de pièges ici !


!!! warning "De manière générale"
    1. Définition simplifiée d'**itérable** : que l'on peut utiliser dans une boucle `#!python for`.
        - Par exemple un objet de type `#!python str`, `#!python tuple` ou `#!python list` est itérable.

    2. La syntaxe Python générale d'une création de liste en compréhension est :
      
        ```python
        <nom_liste> = [ <élément(it)> for <it> in <itérable> if <condition(it)> ]
        ```

    Elle permet de créer une liste d'images (au sens des fonctions en mathématiques) depuis une liste d'antécédents, et avec la possibilité de filtrer suivant une condition.

Voyons des exemples progressifs.


=== "Liste de constantes"

    ```pycon
    >>> [0 for _ in range(5)]
    [0, 0, 0, 0, 0]
    ```

    Une **bonne façon** de créer une liste remplie de $0$.

=== "Liste des lettres d'une string"

    ```pycon
    >>> [lettre for lettre in "azerty tty"]
    ['a', 'z', 'e', 'r', 't', 'y', 't', 't', 'y']
    ```

    L'itérable peut être une chaine, une liste, ou un `#!python range`, ou tout itérable... Voilà une bonne méthode pour convertir un objet en liste.

=== "Liste d'images"

    ```pycon
    >>> [i*i for i in range(6)]
    [0, 1, 4, 9, 16, 25]
    ```
    
    La liste des 6 premiers carrés.

=== "Liste d'images filtrées"

    ```pycon
    >>> [i*i for i in range(20) if i * i % 10 == 9]
    [9, 49, 169, 289]
    ```

    La liste des 20 premiers carrés, filtrée en ne conservant que ceux qui se terminent par un 9.

    C'est **la bonne méthode** à essayer d'utiliser le plus possible. On appelle ce style de programmation, le style fonctionnel, il fait penser fortement à une écriture mathématique.

### Avec du hasard

!!! tip "`randrange`"
    ```pycon
    >>> from random import randrange
    >>> randrange(100)
    47
    >>> randrange(1000, 1050)
    1023
    ```

    La fonction `randrange` du module `random` renvoie un entier au hasard dans l'intervalle qui serait décrit par la fonction `range` (avec 1, deux ou trois paramètres).

    - `#!python randrange(n)` renvoie un entier au hasard parmi les entiers de $0$ inclus à $n$ **exclu**.
    - `#!python randrange(n, m)` renvoie un entier au hasard parmi les entiers de $n$ inclus à $m$ **exclu**.
    - `#!python randrange(n, m, p)` renvoie un entier au hasard parmi les entiers $n$ inclus à $m$ **exclu**, par pas de $p$.

!!! warning "`randint` ou `randrange` ?"
    - On voit souvent `#!python randint(a, b)` qui renvoie un entier de $a$ inclus à $b$ **inclus**.
    - Il existe aussi `#!python randrange(a, b)` qui renvoie un entier de $a$ inclus à $b$ **exclu**.


On peut créer en compréhension une liste d'entiers au hasard. (On parle de nombres pseudo-aléatoires ; le hasard parfait est délicat à obtenir...)

```pycon
>>> [randrange(100) for _ in range(8)]
[65, 14, 69, 25, 78, 22, 10, 17]
```

## Indices

|Indice|0|1|2|3|4|5|6|7|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|Valeur|65|14|69|25|78|22|10|17|


Les tableaux sont indicés à partir de zéro.

```pycon
>>> hasard = [65, 14, 69, 25, 78, 22, 10, 17]
>>> hasard[3]
25
>>> jours_semaine = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
>>> jours_semaine[0]
'lundi'
>>> nb_premiers = [2, 3, 5, 7, 11]
>>> nb_premiers[3]
7
```

Les tableaux sont indicés de zéro, jusqu'à la longueur **exclue**.

```pycon
>>> nb_premiers = [2, 3, 5, 7, 11]
>>> nb_premiers[4]
11
>>> nb_premiers[5]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: list index out of range
```

`nb_premiers` est de longueur $5$. Les indices vont de $0$ inclus à $5$ **exclu**.


## Modification

On a pu lire ; on peut écrire de manière analogue.

```pycon
>>> nb_premiers = [2, 3, 5, 7, 11]
>>> nb_premiers[2]
5
>>> nb_premiers[2] = 43
>>> nb_premiers
[2, 3, 43, 7, 11]
```

Le tableau a été modifié.


## Itération

On peut itérer sur un tableau de deux manières différentes.

- On pourra lire et écrire avec la première méthode.
- On ne pourra que lire avec la première méthode.


=== "Itération avec indice"

    ```python
    nb_elements = 4
    hauteurs = [1.82, 1.75, 1.68, 1.89]
    somme = 0.0
    for i in range(nb_elements):
        somme = somme + hauteurs[i]
    print("La hauteur totale est", somme)
    ```

    ```output
    La hauteur totale est 7.14
    ```

=== "Itération sans indice"

    ```python
    hauteurs = [1.82, 1.75, 1.68, 1.89]
    somme = 0.0
    for h in hauteurs:
        somme = somme + h
    print("La hauteur totale est", somme)
    ```

    ```output
    La hauteur totale est 7.14
    ```

On constate que la deuxième méthode est bien plus simple. Elle est très utile lorsqu'on n'a pas besoin de l'indice pendant la boucle, par exemple pour modifier le tableau.