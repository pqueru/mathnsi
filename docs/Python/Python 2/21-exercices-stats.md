# 📉 Exercices - Statistiques 

Faire des statistiques élémentaires est un excellent moyen de s'entrainer avec les premiers algorithmes.

{{ python_carnet('scripts/stats-sujet.ipynb', module='scripts/stats-teste.py') }}

??? done "Corrigé"

    {{ python_carnet('scripts/stats-correction.ipynb', module='scripts/stats-teste.py') }}