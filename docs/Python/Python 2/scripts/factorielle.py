def factorielle(n):
    """ Renvoie n!, le produit des entiers de 1 à n.
    """
    if n == 0:
        return 1
    else:
        return factorielle(...) * ...

def message():
    print("D'après A002982 :")
    for n in [3, 4, 6, 7, 12, 14]:
        print(f"Pour n = {n}, on a :")
        print(f"     n! - 1 = {factorielle(n) - 1} qui est un nombre premier")
        print()
    print()
    print("D'après A002981 :")
    for n in [0, 1, 2, 3, 11, 27]:
        print(f"Pour n = {n}, on a :")
        print(f"     n! + 1 = {factorielle(n) + 1} qui est un nombre premier")
        print()
