def translation(point_P, vecteur_v):
    """Renvoie le `point_M` : 
    la translation de `point_P` par `vecteur _v`
    
    >>> translation((7, 8), (-2, 5))
    (5, 13)
    
    """
    point_P_x, point_P_y = point_P
    ...
    # à vous ici !



# Test
point_P   = (7, 8)
vecteur_v = (-2, 5)
point_M   = (5, 13)

assert translation(point_P, vecteur_v) == point_M
print("Bravo")