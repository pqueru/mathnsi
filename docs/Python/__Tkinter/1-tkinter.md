# Tkinter

**Tkinter (Tool Kit Interface)** est le module graphique libre d'origine sur Python, permettant la création d'interfaces graphiques.

Il s'agit d'un GUI (Graphical User Interface).

Documentation officielle :  
[https://docs.python.org/fr/3/library/tkinter.html]  
[https://wiki.python.org/moin/TkInter]

Nous allons construire cette interface à l'aide de fenêtres et de widgets.

## Premiers pas - la fenêtre
Saisissez, analyser et testez ce code :
```python linenums="1"
# Import bibliothèque
import tkinter as tk

# Création de l'objet fenêtre
fenetre = tk.Tk()

# Titre de la fenêtre
fenetre.title("Ma super fenêtre !")
# Icone de fenêtre
fenetre.iconbitmap("/images/logo.ico")

# Boucle d'affichage
fenetre.mainloop()
```

Nous créons un objet de type tkinter nommé fenetre et nous lançons une boucle infinie qui attend l'interaction de l'utilisateur.

### Configuration de la fenêtre
```python linenums="1"
# Import bibliothèque
import tkinter as tk

# Création de l'objet fenêtre
fenetre = tk.Tk()

# Titre de la fenêtre
fenetre.title("Ma super fenêtre !")
# Icone de fenêtre
fenetre.iconbitmap("/images/logo.ico")

# Configuration du fond d'écran
fenetre.config(background='#CCCCCC')
# Rendre possible ou non le redimensionnement de la fenêtre
fenetre.resizable(width=True, height=False)

# Boucle d'affichage
fenetre.mainloop()
```

## Les premiers widgets
De nombreux composants graphiques (ou widgets) sont disponibles dans le module Tkinter :  
- **fenêtre** (classe `Tk`)  
- **case à cocher** (classe `Checkbutton`)  
- **zone de texte à saisir** (classe `Entry`)  
- **zone graphique** (classe `Canvas`)  
- **étiquette** (classe `Label`)  
- **bouton** (classe `Button`)  
- **menu** (classe `Menu`)  
- **cadre** (classe `Frame`)  
- ...

On peut également gérer de nombreux événements :  
- clic sur la souris  
- déplacement de la souris  
- survol de la souris  
- appui sur une touche du clavier  
- ...


## Widgets Label, Entry et Button
### Ajout d'un texte avec le widget `Label`
1. Création d'une instance de `tk.Label` associée à la fenêtre en cours
2. Ajout du contenu du texte avec le paramètre `text`  
3. Réglage de la hauteur de l''etiquette avec `height` en nombre de lignes (et éventuellement de la largeur avec `width`)  
4. Options de style (couleurs, police ...)
5. Ajout du texte à la fenêtre avec la méthode `pack`

```python linenums="1"
# Import bibliothèque
import tkinter as tk

# Création de l'objet fenêtre
fenetre = tk.Tk()

# Titre de la fenêtre
fenetre.title("Ma super fenêtre !")
# Icone de fenêtre
fenetre.iconbitmap("/images/logo.ico")

# Configuration du fond d'écran
fenetre.config(background='#CCCCCC')
# Rendre possible ou non le redimensionnement de la fenêtre
fenetre.resizable(width=True, height=False)

# Ajour d'un titre
label_titre = tk.Label(fenetre,
                       text="Mon titre",
                       height=1,
                       fg='black',
                       font=("Calibri", 18),
                       bg="#550000")
# Ajout du texte à la fenêtre
label_titre.pack()

# Boucle d'affichage
fenetre.mainloop()
```

### Ajout d'un bouton avec le widget `Button`

=== "Button"

    1. Création d'une instance de `tk.Button` associée à la fenêtre en cours
    2. Ajout du texte du bouton avec le paramètre `text`  
    3. Commande lancée lors d'un clic sur le bouton  
    4. Options de style (couleurs, police ...)  
    5. Ajout du texte à la fenêtre avec la méthode `pack`

    ```python linenums="1"
    # Import bibliothèque
    import tkinter as tk

    # Création de l'objet fenêtre
    fenetre = tk.Tk()

    # Titre de la fenêtre
    fenetre.title("Ma super fenêtre !")
    # Icone de fenêtre
    fenetre.iconbitmap("/images/logo.ico")

    # Configuration du fond d'écran
    fenetre.config(background='#CCCCCC')
    # Rendre possible ou non le redimensionnement de la fenêtre
    fenetre.resizable(width=True, height=False)

    # Ajour d'un titre
    label_titre = tk.Label(fenetre,
                        text="Mon titre",
                        height=1,
                        fg='black',
                        font=("Calibri", 18),
                        bg="#550000",
                        relief=tk.SUNKEN))
    # Ajout du texte à la fenêtre
    label_titre.pack()

    # Création d'un bouton quitter
    bouton_quitter = tk.Button(fenetre,
                            text="Quitter",
                            command=fenetre.destroy)
    # Ajout du bouton à la fenêtre
    bouton_quitter.pack()

    # Boucle d'affichage
    fenetre.mainloop()
    ```

=== "Options de style"

    + Police et taille  
    + Couleur de la police  
    + Couleur du fond  
    + Relief (RAISED, FLAT, SUNKEN, GROOVE, RIDGE)
    + Ajout du bouton en utilisant toute la place (expand à YES)

    ```python linenums="1"
    # Import bibliothèque
    import tkinter as tk

    # Création de l'objet fenêtre
    fenetre = tk.Tk()

    # Titre de la fenêtre
    fenetre.title("Ma super fenêtre !")
    # Icone de fenêtre
    fenetre.iconbitmap("/images/logo.ico")

    # Configuration du fond d'écran
    fenetre.config(background='#CCCCCC')
    # Rendre possible ou non le redimensionnement de la fenêtre
    fenetre.resizable(width=True, height=False)

    # Ajour d'un titre
    label_titre = tk.Label(fenetre,
                        text="Mon titre",
                        height=1,
                        fg='black',
                        font=("Calibri", 18),
                        bg="#550000",
                        relief=tk.SUNKEN))
    # Ajout du texte à la fenêtre
    label_titre.pack()

    # Création d'un bouton quitter
    bouton_quitter = tk.Button(fenetre,
                            text="Quitter",
                            font=("Calibri", 18),
                            fg='black',
                            bg="#550000",
                            relief=tk.GROOVE,
                            command=fenetre.destroy)
    # Ajout du bouton à la fenêtre
    bouton_quitter.pack(expand=tk.YES)

    # Boucle d'affichage
    fenetre.mainloop()
    ```

### Ajout d'une image avec le widget `Canvas`

1. Création d'une instance de `tk.Canvas` associée à la fenêtre en cours
2. Ajout des options
3. Ajout du canevas à la fenêtre avec la méthode `pack`  
4. Récupération de l'image et réglage du zoom avec `tk.PhotoImage`  
5. Ajout de l'image au canevas avec la méthode `create_image`  

```python linenums="1"
# Import bibliothèque
import tkinter as tk

# Création de l'objet fenêtre
fenetre = tk.Tk()

# Titre de la fenêtre
fenetre.title("Ma super fenêtre !")
# Icone de fenêtre
fenetre.iconbitmap("/images/logo.ico")

# Configuration du fond d'écran
fenetre.config(background='#CCCCCC')
# Rendre possible ou non le redimensionnement de la fenêtre
fenetre.resizable(width=True, height=False)

# Ajour d'un titre
label_titre = tk.Label(fenetre,
                    text="Mon titre",
                    height=1,
                    fg='black',
                    font=("Calibri", 18),
                    bg="#550000",
                    relief=tk.SUNKEN))
# Ajout du texte à la fenêtre
label_titre.pack()

# Création d'un bouton quitter
bouton_quitter = tk.Button(fenetre,
                        text="Quitter",
                        command=fenetre.destroy)
# Ajout du bouton à la fenêtre
bouton_quitter.pack()

# Création du canevas
largeur = 300
hauteur = 200
canevas = tk.Canvas(fenetre, width=largeur, height=hauteur, bg='white')
canevas.pack()
# Récupération et ajout de l'image
img = tk.PhotoImage(file="image/mon_image.jpg").zoom(1)
canevas.create_image(largeur//2, hauteur//2, image=img)

# Boucle d'affichage
fenetre.mainloop()
```

### Positionnement des widgets

Pour placer les widgets, il existe 3 méthodes différentes :  
- `pack` : simple mais automatique, donc peu de réglages possibles  
- `place` : choix de la position, plus précis mais plus dur à gérer et ne s'adapte pas à la fenêtre en cas de redimensionnement  
- `grid` : création de lignes et de colonnes sur lesquels on place les objets dont la taille dépend de la place  

La méthode `grid` est un bon compromis entre les possibilités de chacune.

=== "grid"
    Positionnement sur des lignes et des colonnes sur lesquels on place les objets dont la taille dépend de la place.  
    La numérotation commence à 0.

    L'option `columnspan` permet de fusionner des colonnes.

    L'option `rowspan` permet de fusionner des lignes.

    - **`sticky`** : cette option détermine la façon de distribuer l'espace inoccupé par un widget à l'intérieur d'une cellule.  
    Par défaut, le widget est centré dans sa cellule.  
    On peut sinon utiliser les variables `N+E`, `S+E`, `S+W` ou `N+W`

    - **ipadx** et **ipady**  : marge interne horizontale (resp. verticale). Cette dimension est ajoutée à l'intérieur du widget sur ses côtés gauche et droit (resp. haut et bas)  

    - **padx** et **pady** : marges externes

=== "pack"
    Positionnement à l'aide de l'option `side` et des constantes `tk.BOTTOM`, `tk.TOP`, `tk.RIGHT` et `tk.LEFT`

=== "place"
    Positionnement à l'aide des options `x` et `y` pour l'abscisse et l'ordonnée avec l'origine du repère qui se trouve en haut à gauche de la fenêtre

??? example "Exemple complet"
    ```python linenums="1"
    # Import bibliothèque
    import tkinter as tk

    # Création de l'objet fenêtre
    fenetre = tk.Tk()

    # Titre de la fenêtre
    fenetre.title("Ma super fenêtre")
    # Icone de fenêtre
    fenetre.iconbitmap("images/logo.ico")

    # Configuration du fond d'écran
    fenetre.config(background='#CCCCCC')
    # Rendre possible ou non le redimensionnement de la fenêtre
    fenetre.resizable(width=True, height=False)

    # Création d'un titre
    label_titre = tk.Label(fenetre,
                        text="Mon titre",
                        height=1,
                        fg='black',
                        font=("Calibri", 18),
                        bg="#550000",
                        relief=tk.SUNKEN)
    # Ajout du texte à la fenêtre
    label_titre.grid(row=0, columnspan=2, padx=10, pady=15)

    # Création d'un bouton quitter
    bouton_quitter = tk.Button(fenetre,
                            text="Quitter",
                            font=("Calibri", 18),
                            fg='black',
                            bg="#550000",
                            relief=tk.GROOVE,
                            command=fenetre.destroy)
    # Ajout du bouton à la fenêtre
    bouton_quitter.grid(row=2, column=0, pady=20)

    # Création du canevas
    largeur = 300
    hauteur = 200
    canevas = tk.Canvas(fenetre, width=largeur, height=hauteur, bg='white')
    canevas.grid(row=1, columnspan=2)
    # Récupération et ajout de l'image
    img = tk.PhotoImage(file="images/mon_image.png").zoom(1)
    canevas.create_image(largeur//2, hauteur//2, image=img)

    # Bouton Aide
    bouton_aide = tk.Button(fenetre,
                            text="Aide",
                            font=("Calibri", 18),
                            fg='black',
                            bg='#550000',
                            relief=tk.GROOVE)
    bouton_aide.grid(row=2, column=1, ipadx=10, ipady=10)

    # Bouton Reset
    bouton_reset = tk.Button(fenetre,
                            text="Réinitialiser",
                            font=("Calibri", 18),
                            fg='black',
                            bg="#550000",
                            relief=tk.GROOVE)
    bouton_reset.grid(row=3, column=0)

    # Bouton Sauvegarde
    bouton_sauvegarde = tk.Button(fenetre,
                            text="Sauvegarder",
                            font=("Calibri", 18),
                            fg='black',
                            bg="#550000",
                            relief=tk.GROOVE)
    bouton_sauvegarde.grid(row=3, column=1)

    # Boucle d'affichage
    fenetre.mainloop()
    ```

## Variables de contrôle
Ce sont des attributs pour lesquels on pourra utiliser les méthodes `get` et `set`

### Syntaxe :  
- `val = tk.DoubleVar` : permet de mémoriser un flottant (`0.0` par défaut)  
- `val = tk.IntVar` : permet de mémoriser un entier (`0` par défaut)  
- `val = tk.StringVar` : permet de mémoriser une chaîne de caractères (`''` par défaut)  

Tous les widgets reliés à une variable de contrôle sont automatiquement mis à jour quand une variable est modifiée quand la boucle principale sera à nouveau exécutée.

### Utilisation
1. Définition de la variable `txt_bouton` et affectation avec la méthode `set`  
2. Utilisation de l'option `textvariable` pour utiliser une `StringVar`. À chaque modification de cette variable, le texte du bouton est mis à jour.  
3. Mise à jour du texte du bouton avec la méthode `set` une fois le premier clic sur le bouton effectué.  

```python linenums="1"
def si_clic_bouton():
    ''' Action si clic sur le bouton '''
    txt_bouton.set('Vous avez déjà cliqué sur le bouton')

# Texte du bouton
txt_bouton = tk.StringVar()
txt_bouton.set('Mon bouton')
# Définition du bouton
mon_bouton = tk.Button(fenetre,
                        textvariable=txt_bouton,
                        bg='#dee5dc',
                        bd=2,
                        command=si_clic_bouton)
# Positionnement du bouton
mon_bouton.place(x=50, y=20)
```

## Widget `Entry`
1. Création de la variable de contrôle `choix_utlisateur` et initialisation du contenu  
2. Création d'une instance de `tk.Entry` associée à la fenêtre en cours  
3. Option `textvariable` pour mémoriser la saisie dans `choix_utilisateur`  
4. Ajout des options  
5. Ajout de la zone de saisie à la fenêtre  

```python linenums="1"
# Variable mémorisée par l'instance Entry
choix_utilisateur = tk.StringVar()
choix_utilisateur.set('Saise libre')
# Création de la zone de saisie
zone_saisie = tk.Entry(fenetre,
                        textvariable=choix_utilisateur,
                        width=30,
                        bg='#000000',
                        fg='#00FFFF')
# Positionnement de la zone de saisie
zone_saisie.grid(row=2, column=1)
```

## Ajout d'un cadre avec le widget `Frame` ou `LabelFrame`
1. Création d'une instance de `tk.LabelFrame` associée à la fenêtre en cours  
2. Option `text` permettant d'ajouter du texte en en-tête du cadre  
3. Ajout des options  
4. Ajout du cadre à la fenêtre et positionnement avec `grid`  

```python linenums="1"
# Création du cadre
frame_couleur = tk.LabelFrame(fenetre,
                        width=300,
                        height=40,
                        text="Couleur",
                        font=('Calibri', 11, 'italic'),
                        bg='#CCCCCC',
                        labelanchor='nw')
# Positionnement du cadre
frame_couleur.grid(row=2, column=2, padx=10)
```

## Ajout de boutons radio avec le widget `Radiobutton`
1. Initialisation des valeurs des étiquettes des boutons radios (deux listes)  
2. Création de la variable de contrôle correspondant à la saisie utilisateur  
3. Création d'une instance de `tk.Radiobutton` associée **au cadre "couleur"**  
4. Ajout texte et valeur pour chaque bouton  
5. Ajout des options  
6. Ajout de chaque bouton les uns derrière les autres alignés à gauche avec `pack` (`expand=1` pour prendre toute la largeur)  

```python linenums="1"
# Valeur des puces
valeurs = ['red', 'green', 'blue']
# Étiquette des puces
etiquettes = ['rouge', 'vert', 'bleu']
# Variable de mémorisation du choix utilisateur "couleur"
var_couleur = tk.StringVar()
# Valeur par défaut
var_couleur.set(valeurs[1])
# Remplissage des boutons radio
for i in range(3):
    boutons = tk.Radiobutton(frame_couleur,
                        variable=var_couleur,
                        text=etiquettes[i],
                        value=valeurs[i],
                        bg='#CCCCCC')
    boutons.pack(side=tk.LEFT, expand=1)
```

## Interaction avec Tkinter

Lorsque l'on crée un bouton, on lui associe une fonction qui sera exécutée lors du clic à l'aide de l'option `command` :

```python linenums="1"
# Bouton Haut
bouton_haut = tk.Button(fenetre, text="Haut", command=bouger_haut)
bouton_haut.grid(row=2, column=1, pady=5)
```

On doit donc avoir défini **avant** la création du bouton la fonction associée. Par exemple :

```python linenums="1"
def bouger_haut():
    ''' Déplacer la croix de 5 pts vers le haut '''
    canevas.move(ligne1, 0, -5)
    canevas.move(ligne2, 0, -5)
```

Attention, le repère est orienté vers le bas...

Deuxième exemple : 

```python linenums="1"
def tracer_cercle():
    ''' Tracer un cercle à la position actuelle du curseur '''
    # Récupération de la liste x0, y0, x1, y1 de la croix
    # coordonnées des points en haut à gauche et en bas à droite
    position = canevas.coords(ligne1)
    
    # Création d'un cercle correspondant à la croix
    canevas.create_oval(position[0] - 5, position[1] - 5,
                        position[2] + 5, position[3] + 5,
                        outline='red', width=1)
    
    # Changement de la couleur de la croix
    canevas.itemconfig(ligne1, fill='blue')
    canevas.itemconfig(ligne2, fill='blue')
```

### Modification des items
Voici quelques méthodes disponibles :

1. `delete(nom)` : supprime l'item `nom` passé en paramètre (ou tous les items si utilisation de la constante `tk.ALL`)  
2. `itemconfig(nom, parametre)` : modifie le ou les paramètres spécifiés de l'item `nom`  
3. `itemcget(nom, parametre)` : retourne la valeur actuelle du paramètre `parametre` de l'item `nom`  

??? example "Exemple"

    ```python linenums="1"
    # Changer la couleur
    if canevas.itemcget(ligne1, 'fill') == 'blue':
        # changement de la couleur
        canevas.itemconfig(ligne1, fill='black')
    ```

### Gestion de la souris
#### Action associée au clic droit de la souris

1. Attente du clic droit (bouton 3) de la souris sur le canevas.  
2. Appel de la fonction `clic_droit_souris` par le gestionnaire d'événements
3. La fonction `clic_droit_souris` permet de :  
    + récupérer l'événement dans le paramètre `event`  
    + tracer un tracer au point du clic : clic au point `(event.x, event.y)`

!!! example "Exemple"
    ```python linenums="1"
    def clic_droit_souris(event):
        ''' Gestion du clic droit de la souris '''
        # event.x et event.y contiennent les coordonnées du clic effectué
        # Création d'un cercle correspondant à la croix
        canevas.create_oval(event.x - 10, event.y - 10,
                            event.x + 10, event.y + 10,
                            outline='green', width=2)

    # Attente du clic droit de la souris
    canevas.bind("<Button-3>", clic_droit_souris)
    ```

### Gestion du clavier
!!! example "Exemple"
    ```python linenums="1"
    # Attente de l'appui sur la touche Up du clavier
    fenetre.bind("<KeyPress-Up>", bouger_haut)
    ```

On peut combiner différents événements :

+ `<Button-1>` : l'utilisateur a appuyé sur le premier bouton de la souris (celui de gauche)  
+ `Double` : indique qu'un événement s'est produit 2 fois dans un court laps de temps. Par exemple, `<Double-Button-1>` indique un double clic sur le bouton gauche de la souris  
+ `<KeyRelease-A>` : l'utilisateur a relâché la touche A  
+ `<Control-Shift-KeyPress-B>` : l'utilisateur a appuyé simultanément sur les touches ++ctrl++, ++maj++ et ++B++


## Encapsulation dans une classe

??? example
    ```python linenums="1"
    # Import bibliothèque
    import tkinter as tk

    class Application(tk.Tk):
        def __init__(self):
            # Création de l'objet fenêtre
            tk.Tk.__init__(self)
            
            # Définition des attributs
            # Dimension zone graphique
            self.width_graphe = 750
            self.height_graphe = 500

            # Titre de la fenêtre
            self.title("Ma super fenêtre")
            # Icone de fenêtre
            self.iconbitmap("images/logo.ico")

            # Configuration du fond d'écran
            self.config(background='#CCCCCC')
            # Rendre possible ou non le redimensionnement de la fenêtre
            self.resizable(width=True, height=False)

            # Création d'un titre
            label_titre = tk.Label(self,
                                text="Tracer des cercles",
                                height=1,
                                fg='white',
                                font=("Calibri", 18),
                                bg="#550000",
                                relief=tk.SUNKEN)
            # Ajout du texte à la fenêtre
            label_titre.grid(row=0, columnspan=4, padx=10, pady=15)
            
            self.creer_titres()
            
            self.creer_boutons()
            
            self.creer_zone_graphique()
            
            # Attente du clic droit de la souris
            self.canevas.bind("<Button-1>", self.clic_droit_souris)

            # Attente de l'appui sur la touche Up du clavier
            self.bind("<KeyPress-Up>", self.bouger_haut)
            self.bind("<KeyPress-Down>", self.bouger_bas)
            self.bind("<KeyPress-Left>", self.bouger_gauche)
            self.bind("<KeyPress-Right>", self.bouger_droite)

        def creer_titres(self):
            # Création d'un titre
            label_titre = tk.Label(self,
                                text="Tracer des cercles",
                                height=1,
                                fg='white',
                                font=("Calibri", 18),
                                bg="#550000",
                                relief=tk.SUNKEN)
            # Ajout du texte à la fenêtre
            label_titre.grid(row=0, columnspan=4, padx=10, pady=15)
        
        def change_couleur_croix(self):
            # Changer la couleur
            if self.canevas.itemcget(self.ligne1, 'fill') == 'blue':
                # changement de la couleur
                self.canevas.itemconfig(self.ligne1, fill='black')
            if self.canevas.itemcget(self.ligne2, 'fill') == 'blue':
                # changement de la couleur
                self.canevas.itemconfig(self.ligne2, fill='black')
            
        def clic_droit_souris(self, *args):
            ''' Gestion du clic droit de la souris '''
            # event.x et event.y contiennent les coordonnées du clic effectué
            # Création d'un cercle correspondant à la croix
            event = args[0]
            self.canevas.create_oval(event.x - 10, event.y - 10,
                                event.x + 10, event.y + 10,
                                outline='green', width=2)

        def creer_zone_graphique(self):
            self.frame_graphe = tk.Frame(self, relief=tk.GROOVE, borderwidth=2,
                                        width=self.width_graphe, height=self.height_graphe)
            
            # Création du canevas
            self.canevas = tk.Canvas(self, width=self.width_graphe, height=self.height_graphe, bg='ivory')
            self.canevas.grid(row=1, columnspan=4, padx=20, ipady=20)

            # Tracer une croix au centre du canevas
            self.ligne1 = self.canevas.create_line(self.largeur//2 - 5, self.hauteur//2 - 5,
                                    self.largeur//2 + 5, self.hauteur//2 + 5,
                                    fill="black", width=5)
            self.ligne2 = self.canevas.create_line(self.largeur//2 - 5, self.hauteur//2 + 5,
                                    self.largeur//2 + 5, self.hauteur//2 - 5,
                                    fill='black', width=5)

        def tracer_cercle(self):
            ''' Tracer un cercle à la position actuelle du curseur '''
            # Récupération de la liste x0, y0, x1, y1 de la croix
            # coordonnées des points en haut à gauche et en bas à droite
            position = self.canevas.coords(self.ligne1)
            
            # Création d'un cercle correspondant à la croix
            self.canevas.create_oval(position[0] - 5, position[1] - 5,
                                position[2] + 5, position[3] + 5,
                                outline='red', width=1)
            
            # Changement de la couleur de la croix
            self.canevas.itemconfig(self.ligne1, fill='blue')
            self.canevas.itemconfig(self.ligne2, fill='blue')

        def bouger_haut(self, *args):
            ''' Déplacer la croix de 5 pts vers le haut '''
            self.change_couleur_croix()
            self.canevas.move(self.ligne1, 0, -5)
            self.canevas.move(self.ligne2, 0, -5)

        def bouger_bas(self, *args):
            ''' Déplacer la croix de 5 pts vers le bas '''
            self.change_couleur_croix()
            self.canevas.move(self.ligne1, 0, 5)
            self.canevas.move(self.ligne2, 0, 5)

        def bouger_gauche(self, *args):
            ''' Déplacer la croix de 5 pts vers la gauche '''
            self.change_couleur_croix()
            self.canevas.move(self.ligne1, -5, 0)
            self.canevas.move(self.ligne2, -5, 0)

        def bouger_droite(self, *args):
            ''' Déplacer la croix de 5 pts vers la droite '''
            self.change_couleur_croix()
            self.canevas.move(self.ligne1, 5, 0)
            self.canevas.move(self.ligne2, 5, 0)

        def creer_boutons(self):
            
            # Bouton Tracer
            bouton_tracer = tk.Button(self, text="Tracer", command=self.tracer_cercle)
            bouton_tracer.grid(row=2, column=3, pady=5)

            # Création d'un bouton quitter
            bouton_quitter = tk.Button(self, text="Quitter", command=self.destroy)
            # Ajout du bouton à la fenêtre
            bouton_quitter.grid(row=3, column=3, pady=5)

            # Bouton Haut
            bouton_haut = tk.Button(self, text="Haut", command=self.bouger_haut)
            bouton_haut.grid(row=2, column=1, pady=5)

            # Bouton Bas
            bouton_bas = tk.Button(self, text="Bas", command=self.bouger_bas)
            bouton_bas.grid(row=3, column=1, pady=5)

            # Bouton Gauche
            bouton_gauche = tk.Button(self, text="Gauche", command=self.bouger_gauche)
            bouton_gauche.grid(row=2, rowspan=2, column=0)

            # Bouton Droite
            bouton_droite = tk.Button(self, text="Droite", command=self.bouger_droite)
            bouton_droite.grid(row=2, rowspan=2, column=2)

    if __name__ == "__main__":
        app = Application()
        app.mainloop()

    ```