# Import bibliothèque
import tkinter as tk

# Création de l'objet fenêtre
fenetre = tk.Tk()

# Titre de la fenêtre
fenetre.title("Pascal")
# Icone de fenêtre
fenetre.iconbitmap("images/logo.ico")

# Configuration du fond d'écran
fenetre.config(background='#CCCCCC')
# Rendre possible ou non le redimensionnement de la fenêtre
fenetre.resizable(width=True, height=False)

# Création d'un titre
label_titre = tk.Label(fenetre,
                       text="Tracer des cercles",
                       height=1,
                       fg='white',
                       font=("Calibri", 18),
                       bg="#550000",
                       relief=tk.SUNKEN)
# Ajout du texte à la fenêtre
label_titre.grid(row=0, columnspan=4, padx=10, pady=15)


# Création du canevas
largeur = 300
hauteur = 200
canevas = tk.Canvas(fenetre, width=largeur, height=hauteur, bg='ivory')
canevas.grid(row=1, columnspan=4, padx=20, ipady=20)

# Tracer une croix au centre du canevas
ligne1 = canevas.create_line(largeur//2 - 5, hauteur//2 - 5,
                         largeur//2 + 5, hauteur//2 + 5,
                         fill="black", width=5)
ligne2 = canevas.create_line(largeur//2 - 5, hauteur//2 + 5,
                         largeur//2 + 5, hauteur//2 - 5,
                         fill='black', width=5)

def change_couleur_croix():
    # Changer la couleur
    if canevas.itemcget(ligne1, 'fill') == 'blue':
        # changement de la couleur
        canevas.itemconfig(ligne1, fill='black')
    if canevas.itemcget(ligne2, 'fill') == 'blue':
        # changement de la couleur
        canevas.itemconfig(ligne2, fill='black')

def tracer_cercle():
    ''' Tracer un cercle à la position actuelle du curseur '''
    # Récupération de la liste x0, y0, x1, y1 de la croix
    # coordonnées des points en haut à gauche et en bas à droite
    position = canevas.coords(ligne1)
    
    # Création d'un cercle correspondant à la croix
    canevas.create_oval(position[0] - 5, position[1] - 5,
                        position[2] + 5, position[3] + 5,
                        outline='red', width=1)
    
    # Changement de la couleur de la croix
    canevas.itemconfig(ligne1, fill='blue')
    canevas.itemconfig(ligne2, fill='blue')

def bouger_haut(event):
    ''' Déplacer la croix de 5 pts vers le haut '''
    change_couleur_croix()
    canevas.move(ligne1, 0, -5)
    canevas.move(ligne2, 0, -5)

def bouger_bas(event):
    ''' Déplacer la croix de 5 pts vers le bas '''
    change_couleur_croix()
    canevas.move(ligne1, 0, 5)
    canevas.move(ligne2, 0, 5)

def bouger_gauche(event):
    ''' Déplacer la croix de 5 pts vers la gauche '''
    change_couleur_croix()
    canevas.move(ligne1, -5, 0)
    canevas.move(ligne2, -5, 0)

def bouger_droite(event):
    ''' Déplacer la croix de 5 pts vers la droite '''
    change_couleur_croix()
    canevas.move(ligne1, 5, 0)
    canevas.move(ligne2, 5, 0)
    
def clic_droit_souris(event):
    ''' Gestion du clic droit de la souris '''
    # event.x et event.y contiennent les coordonnées du clic effectué
    # Création d'un cercle correspondant à la croix
    canevas.create_oval(event.x - 10, event.y - 10,
                        event.x + 10, event.y + 10,
                        outline='green', width=2)

# Attente du clic droit de la souris
canevas.bind("<Button-1>", clic_droit_souris)

# Attente de l'appui sur la touche Up du clavier
fenetre.bind("<KeyPress-Up>", bouger_haut)
fenetre.bind("<KeyPress-Down>", bouger_bas)
fenetre.bind("<KeyPress-Left>", bouger_gauche)
fenetre.bind("<KeyPress-Right>", bouger_droite)

# Bouton Tracer
bouton_tracer = tk.Button(fenetre, text="Tracer", command=tracer_cercle)
bouton_tracer.grid(row=2, column=3, pady=5)

# Création d'un bouton quitter
bouton_quitter = tk.Button(fenetre, text="Quitter", command=fenetre.destroy)
# Ajout du bouton à la fenêtre
bouton_quitter.grid(row=3, column=3, pady=5)

# Bouton Haut
bouton_haut = tk.Button(fenetre, text="Haut", command=bouger_haut)
bouton_haut.grid(row=2, column=1, pady=5)

# Bouton Bas
bouton_bas = tk.Button(fenetre, text="Bas", command=bouger_bas)
bouton_bas.grid(row=3, column=1, pady=5)

# Bouton Gauche
bouton_gauche = tk.Button(fenetre, text="Gauche", command=bouger_gauche)
bouton_gauche.grid(row=2, rowspan=2, column=0)

# Bouton Droite
bouton_droite = tk.Button(fenetre, text="Droite", command=bouger_droite)
bouton_droite.grid(row=2, rowspan=2, column=2)


# Boucle d'affichage
fenetre.mainloop()

