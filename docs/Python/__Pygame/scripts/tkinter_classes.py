# Import bibliothèque
import tkinter as tk

class Application(tk.Tk):
    def __init__(self):
        # Création de l'objet fenêtre
        tk.Tk.__init__(self)
        
        # Définition des attributs
        # Dimension zone graphique
        self.width_graphe = 750
        self.height_graphe = 500
        
        # Créatoin des variables de saisie et initialisation
        self.prenom = tk.StringVar()
        

        # Titre de la fenêtre
        self.title("Pascal")
        # Icone de fenêtre
        self.iconbitmap("images/logo.ico")

        # Configuration du fond d'écran
        self.config(background='#CCCCCC')
        # Rendre possible ou non le redimensionnement de la fenêtre
        self.resizable(width=True, height=False)

        # Création d'un titre
        label_titre = tk.Label(self,
                               text="Tracer des cercles",
                               height=1,
                               fg='white',
                               font=("Calibri", 18),
                               bg="#550000",
                               relief=tk.SUNKEN)
        # Ajout du texte à la fenêtre
        label_titre.grid(row=0, columnspan=4, padx=10, pady=15)
        
        self.creer_titres()
        
        self.creer_widgets()
        
        self.creer_boutons()
        
        self.creer_zone_graphique()
        
        # Attente du clic droit de la souris
        self.canevas.bind("<Button-1>", self.clic_droit_souris)

        # Attente de l'appui sur la touche Up du clavier
        self.bind("<KeyPress-Up>", self.bouger_haut)
        self.bind("<KeyPress-Down>", self.bouger_bas)
        self.bind("<KeyPress-Left>", self.bouger_gauche)
        self.bind("<KeyPress-Right>", self.bouger_droite)

    def creer_titres(self):
        # Création d'un titre
        label_titre = tk.Label(self,
                               text="Tracer des cercles",
                               height=1,
                               fg='white',
                               font=("Calibri", 18),
                               bg="#550000",
                               relief=tk.SUNKEN)
        # Ajout du texte à la fenêtre
        label_titre.grid(row=0, columnspan=4, padx=10, pady=15)
    
    def change_couleur_croix(self):
        # Changer la couleur
        if self.canevas.itemcget(self.ligne1, 'fill') == 'blue':
            # changement de la couleur
            self.canevas.itemconfig(self.ligne1, fill='black')
        if self.canevas.itemcget(self.ligne2, 'fill') == 'blue':
            # changement de la couleur
            self.canevas.itemconfig(self.ligne2, fill='black')

    
        
    def clic_droit_souris(self, *args):
        ''' Gestion du clic droit de la souris '''
        # event.x et event.y contiennent les coordonnées du clic effectué
        # Création d'un cercle correspondant à la croix
        event = args[0]
        self.canevas.create_oval(event.x - 10, event.y - 10,
                            event.x + 10, event.y + 10,
                            outline='green', width=2)

    def creer_zone_graphique(self):
        self.frame_graphe = tk.Frame(self, relief=tk.GROOVE, borderwidth=2,
                                     width=self.width_graphe, height=self.height_graphe)
        
        # Création du canevas
        self.largeur = 300
        self.hauteur = 200
        self.canevas = tk.Canvas(self, width=self.largeur, height=self.hauteur, bg='ivory')
        self.canevas.grid(row=1, columnspan=4, padx=20, ipady=20)

        # Tracer une croix au centre du canevas
        self.ligne1 = self.canevas.create_line(self.largeur//2 - 5, self.hauteur//2 - 5,
                                 self.largeur//2 + 5, self.hauteur//2 + 5,
                                 fill="black", width=5)
        self.ligne2 = self.canevas.create_line(self.largeur//2 - 5, self.hauteur//2 + 5,
                                 self.largeur//2 + 5, self.hauteur//2 - 5,
                                 fill='black', width=5)

    def tracer_cercle(self):
        ''' Tracer un cercle à la position actuelle du curseur '''
        # Récupération de la liste x0, y0, x1, y1 de la croix
        # coordonnées des points en haut à gauche et en bas à droite
        position = self.canevas.coords(self.ligne1)
        
        # Création d'un cercle correspondant à la croix
        self.canevas.create_oval(position[0] - 5, position[1] - 5,
                            position[2] + 5, position[3] + 5,
                            outline='red', width=1)
        
        # Changement de la couleur de la croix
        self.canevas.itemconfig(self.ligne1, fill='blue')
        self.canevas.itemconfig(self.ligne2, fill='blue')

    def bouger_haut(self, *args):
        ''' Déplacer la croix de 5 pts vers le haut '''
        self.change_couleur_croix()
        self.canevas.move(self.ligne1, 0, -5)
        self.canevas.move(self.ligne2, 0, -5)

    def bouger_bas(self, *args):
        ''' Déplacer la croix de 5 pts vers le bas '''
        self.change_couleur_croix()
        self.canevas.move(self.ligne1, 0, 5)
        self.canevas.move(self.ligne2, 0, 5)

    def bouger_gauche(self, *args):
        ''' Déplacer la croix de 5 pts vers la gauche '''
        self.change_couleur_croix()
        self.canevas.move(self.ligne1, -5, 0)
        self.canevas.move(self.ligne2, -5, 0)

    def bouger_droite(self, *args):
        ''' Déplacer la croix de 5 pts vers la droite '''
        self.change_couleur_croix()
        self.canevas.move(self.ligne1, 5, 0)
        self.canevas.move(self.ligne2, 5, 0)

    def creer_boutons(self):
        
        # Bouton Tracer
        bouton_tracer = tk.Button(self, text="Tracer", command=self.tracer_cercle)
        bouton_tracer.grid(row=2, column=3, pady=5)

        # Création d'un bouton quitter
        bouton_quitter = tk.Button(self, text="Quitter", command=self.destroy)
        # Ajout du bouton à la fenêtre
        bouton_quitter.grid(row=3, column=3, pady=5)

        # Bouton Haut
        bouton_haut = tk.Button(self, text="Haut", command=self.bouger_haut)
        bouton_haut.grid(row=2, column=1, pady=5)

        # Bouton Bas
        bouton_bas = tk.Button(self, text="Bas", command=self.bouger_bas)
        bouton_bas.grid(row=3, column=1, pady=5)

        # Bouton Gauche
        bouton_gauche = tk.Button(self, text="Gauche", command=self.bouger_gauche)
        bouton_gauche.grid(row=2, rowspan=2, column=0)

        # Bouton Droite
        bouton_droite = tk.Button(self, text="Droite", command=self.bouger_droite)
        bouton_droite.grid(row=2, rowspan=2, column=2)

if __name__ == "__main__":
    app = Application()
    app.mainloop()