# Import bibliothèque
import tkinter as tk

# Création de l'objet fenêtre
fenetre = tk.Tk()

# Titre de la fenêtre
fenetre.title("Pascal")
# Icone de fenêtre
fenetre.iconbitmap("images/logo.ico")

# Configuration du fond d'écran
fenetre.config(background='#CCCCCC')
# Rendre possible ou non le redimensionnement de la fenêtre
fenetre.resizable(width=True, height=False)

# Création d'un titre
label_titre = tk.Label(fenetre,
                       text="Mon titre",
                       height=1,
                       fg='white',
                       font=("Calibri", 18),
                       bg="#550000",
                       relief=tk.SUNKEN)
# Ajout du texte à la fenêtre
label_titre.grid(row=0, columnspan=2, padx=10, pady=15)

# Création d'un bouton quitter
bouton_quitter = tk.Button(fenetre,
                           text="Quitter",
                           font=("Calibri", 18),
                           fg='black',
                           bg="#550000",
                           relief=tk.GROOVE,
                           command=fenetre.destroy)
# Ajout du bouton à la fenêtre
bouton_quitter.grid(row=2, column=0, pady=20)

# Création du canevas
largeur = 300
hauteur = 200
canevas = tk.Canvas(fenetre, width=largeur, height=hauteur, bg='white')
canevas.grid(row=1, columnspan=2)
# Récupération et ajout de l'image
#img = tk.PhotoImage(file="../../images/pyrates.png").zoom(1)
#canevas.create_image(largeur//2, hauteur//2, image=img)

# Bouton Aide
bouton_aide = tk.Button(fenetre,
                           text="Aide",
                           font=("Calibri", 18),
                           fg='black',
                           bg='#550000',
                           relief=tk.GROOVE)
bouton_aide.grid(row=2, column=1, ipadx=10, ipady=10)

# Bouton Reset
bouton_reset = tk.Button(fenetre,
                           text="Réinitialiser",
                           font=("Calibri", 18),
                           fg='black',
                           bg="#550000",
                           relief=tk.GROOVE)
bouton_reset.grid(row=3, column=0)

# Bouton Sauvegarde
bouton_sauvegarde = tk.Button(fenetre,
                           text="Sauvegarder",
                           font=("Calibri", 18),
                           fg='black',
                           bg="#550000",
                           relief=tk.GROOVE)
bouton_sauvegarde.grid(row=3, column=1)

def si_clic_bouton():
    ''' Action si clic sur le bouton '''
    txt_bouton.set('Vous avez déjà cliqué sur le bouton')

# Texte du bouton
txt_bouton = tk.StringVar()
txt_bouton.set('Mon bouton')
# Définition du bouton
mon_bouton = tk.Button(fenetre,
                        textvariable=txt_bouton,
                        bg='#dee5dc',
                        bd=2,
                        command=si_clic_bouton)
# Positionnement du bouton
mon_bouton.place(x=50, y=20)

# Boucle d'affichage
fenetre.mainloop()