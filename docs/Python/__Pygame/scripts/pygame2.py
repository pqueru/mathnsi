from random import randint
import pygame
import time
 
 
SCREEN_WIDTH = 910
SCREEN_HEIGHT = 603

ROUGE = (255, 0, 0)
ROUGE2 = (190, 25, 25)
BLEU = (27, 24, 181)

VITESSE_MISSILE = 2
 
running = True
 
pygame.init()

myfont = pygame.font.SysFont('Comic Sans MS', 30)
MESSAGE_PERDU = myfont.render('GAME OVER!', True, ROUGE2)

# Création de la fenêtre
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
# Affichage de l'arrière plan
fond = pygame.image.load('background.jpg').convert()
img = pygame.image.load("pyg.png")

class Joueur:
    def __init__(self, pos):
        self.image = pygame.image.load('xwing.png')
        self.position = [pos[0], pos[1]]
        self.vitesse = 10
        self.score = 0
        
    def get_position(self):
        return (self.position[0], self.position[1])
    
    def update(self):
        screen.blit(self.image, self.get_position())
        
class Alien():
    def __init__(self):
        self.image = pygame.image.load('space11.png')
        self.position = [randint(0, SCREEN_WIDTH), 0]
        self.vitesse = 1
        
    def get_position(self):
        return (self.position[0], self.position[1])
    
    def update(self):
        if 0 <= self.position[1] < SCREEN_HEIGHT:
            self.position[1] += self.vitesse
            
        screen.blit(self.image, self.get_position())



class Missile:
    cote = True
    def __init__(self, pos):
        self.image = pygame.image.load('lazer1.png')
         
        self.position = self.image.get_rect()
        self.position = [pos[0], pos[1]]
        if Missile.cote:
            self.position[0] += 40
        Missile.cote = not Missile.cote
        
    def get_position(self):
        return (self.position[0], self.position[1])
    
    def update(self):
        if self.position[1] >= -10:
            self.position[1] -= 5
        #print(self.get_position())
        screen.blit(self.image, self.get_position())



def distance(vaisseau1, vaisseau2):
    return (vaisseau1.position[0]-vaisseau2.position[0])**2 + \
            (vaisseau1.position[1]-vaisseau2.position[1])**2
                

def main_events():
    global running, temps
    
    for event in pygame.event.get():
        
        if event.type == pygame.QUIT:
            running = False
       
    clock.tick(20)
    temps += 1
    
    if temps % 80 == 0:
        liste_aliens.append(Alien())
    
    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_LEFT]:
        v1.position[0] -= v1.vitesse
    elif pressed[pygame.K_RIGHT] and v1.position[0] > 0:
        v1.position[0] += v1.vitesse
    elif pressed[pygame.K_UP] and v1.position[0] < SCREEN_WIDTH:
        v1.position[1] -= v1.vitesse
    elif pressed[pygame.K_DOWN] and v1.position[1] > 0:
        v1.position[1] += v1.vitesse
    elif pressed[pygame.K_SPACE] and v1.position[1] < SCREEN_HEIGHT:
        print(len(liste_missiles))
        liste_missiles.append(Missile(v1.get_position()))
    
class Jeu:
    def __init__(self):
        self.temps = 0
        self.joueur = Vaisseau((400, 450))
        self.clock = pygame.time.Clock()
        self.liste_missiles = []
        self.liste_aliens = []
        
    def main_events():
        global running, temps
        
        for event in pygame.event.get():
            
            if event.type == pygame.QUIT:
                running = False
           
        self.clock.tick(20)
        self.temps += 1
        
        if self.temps % 80 == 0:
            self.liste_aliens.append(Alien())
        
        pressed = pygame.key.get_pressed()
        if pressed[pygame.K_LEFT]:
            self.joueur.position[0] -= self.joueur.vitesse
        elif pressed[pygame.K_RIGHT] and self.joueur.position[0] > 0:
            self.joueur.position[0] += self.joueur.vitesse
        elif pressed[pygame.K_UP] and self.joueur.position[0] < SCREEN_WIDTH:
            self.joueur.position[1] -= self.joueur.vitesse
        elif pressed[pygame.K_DOWN] and self.joueur.position[1] > 0:
            self.joueur.position[1] += self.joueur.vitesse
        elif pressed[pygame.K_SPACE] and self.joueur.position[1] < SCREEN_HEIGHT:
            print(len(self.liste_missiles))
            self.liste_missiles.append(Missile(self.joueur.get_position()))
        
    def detect_collision(self):
    for missile in self.liste_missiles:
        for alien in self.liste_aliens:
            if distance(missile, alien) < 400:
                v1.score += 10
                explosion_position = [alien.position[0] + 20, alien.position[1] + 20]
                self.liste_aliens.remove(alien)
                self.liste_missiles.remove(missile)
                explosion = pygame.image.load('Bubble.png')
                pygame.draw.circle(screen, ROUGE, explosion_position, 15, 15)



### Début du jeu
temps = 0
v1 = Vaisseau((400, 450))

clock = pygame.time.Clock()

liste_missiles = []
liste_aliens = []

while running:
    screen.blit(fond, (0,0))
    
    main_events()
    

    screen.blit(fond, (0,0))
    screen.blit(img, (350,50))
    
    screen.blit(v1.image, v1.get_position())
    
    for m in liste_missiles:
        m.update()
    for a in liste_aliens:
        a.update()
    
    detect_collision()
        
    for a in liste_aliens:
        if distance(a, v1) < 400:
            running = False
            screen.blit(MESSAGE_PERDU, (400,300))
            pygame.display.update()
            time.sleep(5)
            
    texte_score = 'Score : ' + str(v1.score)
    message_score = myfont.render(texte_score, True, BLEU)
    screen.blit(message_score, (10,10))
        
    pygame.display.update()

print(texte_score)
pygame.quit()