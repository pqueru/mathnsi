# Import bibliothèque
import tkinter as tk

# Création de l'objet fenêtre
fenetre = tk.Tk()

# Titre de la fenêtre
fenetre.title("Pascal")
# Icone de fenêtre
fenetre.iconbitmap("images/logo.ico")

# Configuration du fond d'écran
fenetre.config(background='#CCCCCC')
# Rendre possible ou non le redimensionnement de la fenêtre
fenetre.resizable(width=True, height=False)




# Variable mémorisée par l'instance Entry
choix_utilisateur = tk.StringVar()
choix_utilisateur.set('Saise libre')
# Création de la zone de saisie
zone_saisie = tk.Entry(fenetre,
                        textvariable=choix_utilisateur,
                        width=30,
                        bg='#000000',
                        fg='#00FFFF')
# Positionnement de la zone de saisie
zone_saisie.grid(row=1, column=1)

# Création du cadre
frame_couleur = tk.LabelFrame(fenetre,
                        width=300,
                        height=40,
                        text="Couleur",
                        font=('Calibri', 11, 'italic'),
                        bg='#CCCCCC',
                        labelanchor='nw')
# Positionnement du cadre
frame_couleur.grid(row=1, column=2, padx=10)

# Valeur des puces
valeurs = ['red', 'green', 'blue']
# Étiquette des puces
etiquettes = ['rouge', 'vert', 'bleu']
# Variable de mémorisation du choix utilisateur "couleur"
var_couleur = tk.StringVar()
# Valeur par défaut
var_couleur.set(valeurs[1])
# Remplissage des boutons radio
for i in range(3):
    boutons = tk.Radiobutton(frame_couleur,
                        variable=var_couleur,
                        text=etiquettes[i],
                        value=valeurs[i],
                        bg='#CCCCCC')
    boutons.pack(side=tk.LEFT, expand=1)

# Boucle d'affichage
fenetre.mainloop()