# Import bibliothèque
import tkinter as tk
from matplotlib import *
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from time import sleep

class Application(tk.Tk):
    def __init__(self):
        # Création de l'objet fenêtre
        tk.Tk.__init__(self)
        #self.geometry("600x400")
        
        # Définition des attributs
        # Dimension zone graphique
        self.width_graphe = 450
        self.height_graphe = 300
        
        # Créatoin des variables de saisie et initialisation
        self.choix_ville = tk.StringVar()

        # Titre de la fenêtre
        self.title("Météo")
        # Icone de fenêtre
        self.iconbitmap("images/logo.ico")

        # Configuration du fond d'écran
        self.config(background='#999999')
        # Rendre possible ou non le redimensionnement de la fenêtre
        self.resizable(width=True, height=False)
        
        self.creer_titres()
        
        self.creer_saisie()
        
        self.creer_boutons()

    def creer_titres(self):
        # Création d'un titre
        self.label_titre = tk.Label(self,
                               text="Bulletin météo",
                               height=1,
                               fg='white',
                               font=("Calibri", 18),
                               bg="#444444",
                               relief=tk.RAISED)
        # Ajout du texte à la fenêtre
        self.label_titre.grid(row=0, column=0, columnspan=5, padx=10, pady=15)
    
    def creer_saisie(self):
        self.frame_saisie = tk.Frame(self,
                                     borderwidth=5,
                                     width=self.width_graphe,
                                     height=100,
                                     bg="#AAAAAA",
                                     relief=tk.GROOVE)
        self.frame_saisie.grid(row=1,
                               column=2,
                               columnspan=3,
                               pady=5,
                               padx=50,
                               sticky="NESW")
        
        # Variable mémorisée par l'instance Entry
        self.choix_ville.set('New York')
        # Création de la zone de saisie
        self.zone_saisie = tk.Entry(self.frame_saisie,
                                textvariable=self.choix_ville,
                                width=10,
                                bg='#FFFFFF',
                                fg='#0022FF')
        # Positionnement de la zone de saisie
        self.zone_saisie.grid(row=0, column=0, padx=10, pady=10)
        

        
    def afficher_meteo():
        pass


    def creer_boutons(self):
        
        # Bouton Recherche
        bouton_recherche = tk.Button(self.frame_saisie,
                                     text="Recherche",
                                     highlightbackground="#CCCCCC",
                                     command=self.afficher_meteo)
        bouton_recherche.grid(row=0, column=1, padx=5)

        # Création d'un bouton quitter
        bouton_quitter = tk.Button(self.frame_saisie,
                                   text="Quitter",
                                   highlightbackground="#CCCCCC",
                                   command=self.destroy)
        # Ajout du bouton à la fenêtre
        bouton_quitter.grid(row=0, column=2, padx=5)

if __name__ == "__main__":
    app = Application()
    app.mainloop()