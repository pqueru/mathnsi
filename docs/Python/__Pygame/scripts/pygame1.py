import pygame

pygame.init()

fenetre = pygame.display.set_mode((800,600))
run = True
img = pygame.image.load("pyg.png")

fond = pygame.image.load("background.jpg").convert()
fenetre.blit(fond, (0,0))

posX = 50
vx = 1

clock = pygame.time.Clock()

while run :
  for event in pygame.event.get():
      if event.type == pygame.QUIT:
          run = False

  clock.tick(60)
  
  if (posX > 770) or (posX < 30) :
    vx = -vx

  posX = posX + vx

  fenetre.blit(fond, (0,0))
  fenetre.blit(img,(200,200))
  pygame.draw.circle(fenetre, (255,0,0), (posX,300), 30, 2)
  pygame.display.flip()
  #pygame.display.update()

pygame.quit()