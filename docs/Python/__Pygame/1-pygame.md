# Pygame
> `Pygame` est une bibliothèque libre multiplate-forme qui facilite le développement de jeux vidéo temps réel avec le langage de programmation `Python`. Elle permet de programmer la partie multimédia (graphismes, son et entrées au clavier, à la souris ou au joystick), sans se heurter aux difficultés des langages de bas niveau. Pygame est distribuée selon les termes de la licence GNU LGPL. (d'après Wikipédia)

## Installation
Dans un terminal, saisissez la ligne suivante :
```terminal
pip install pygame
```

## Premiers pas
Saisissez, analyser et testez ce code :
```python linenums="1"
import pygame

pygame.init()

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.quit()
```

Quelques explications sur le code ci-dessus :

- la première ligne permet d'importer la bibliothèque `Pygame`
- `pygame.init()` initialise les modules de `Pygame`
- `screen = pygame.display.set_mode((800, 600))` permet de créer la `surface` pygame, cette `surface` aura pour dimension 800 pixels de large et 600 pixels de haut
- la dernière ligne permet de quitter proprement votre programme `Pygame` (si vous omettez cette ligne vous risquez de vous retrouver bloqué avec une fenêtre impossible à fermer)

Vous avez peut-être remarqué qu'une fenêtre s'ouvre et se referme quasi immédiatement après. Pourquoi ?

L'interpréteur Python exécute les instructions ligne après ligne, une fois la dernière ligne exécutée, le programme est terminé et la fenêtre se ferme.

!!! danger "Attention"
    Il faut donc empêcher la fenêtre de se refermer, il faut donc empêcher le programme de se terminer. Pour cela nous allons employer une boucle. On appelle souvent cette boucle une "boucle de jeu"

## Boucle de jeu
Saisissez, analysez et testez ce code :

```python linenums="1"
import pygame

pygame.init()

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

pygame.quit()
```

Nous avons bien notre boucle (`while running` avec au départ `running = True`).

Cette boucle ne peut pas continuer indéfiniment, il faut laisser à l'utilisateur la possibilité de sortir du programme. Pour cela nous utilisons le gestionnaire d'événements de `Pygame` :

`Pygame` "surveille" tous les événements qui pourraient survenir (principalement une action de l'utilisateur sur la souris ou sur le clavier). 

!!! info "à retenir"
    `pygame.event.get()` renvoie un tableau avec tous les événements en cours, la boucle `for` permet de parcourir tous ces événements. On retrouve ces événements dans l'objet `event`. Un des événements possibles est "le clic de souris sur la croix en haut à gauche de la fenêtre" traduit par `pygame.QUIT`. Si l'utilisateur clique sur "la croix en haut à gauche de la fenêtre", on "entre" dans le `if` et la variable `running` devient `False` : la "boucle de jeu" se termine, la ligne `pygame.quit()` est exécutée, et le programme se termine.

!!! tip "à noter"
    Il faut bien comprendre que la liste des événements en cours est réactualisée en permanence grâce à la "boucle de jeu" : les instructions contenues dans la boucle sont exécutées des dizaines de fois par seconde, on a donc `pygame.event.get()` qui est exécutée plusieurs dizaines de fois par seconde, la liste des événements est donc mise à jour plusieurs dizaines de fois par seconde ! On peut donc dire que les événements (clavier et souris) sont "surveillés" en "permanence" (même si on devrait plutôt dire que les événements sont "controlés" plusieurs dizaines de fois par seconde).

## Couleur de fond
Pour choisir une couleur de fond, on peut utiliser la méthode `fill` :
```python linenums="1"
VIOLET = (100, 20, 100) # couleur est défini ici par ses attributs RGB
screen.fill(VIOLET)
```

## Dessins
Saisissez, analysez et testez ce code

```python linenums="1"
import pygame

pygame.init()

BLANC = (255, 255, 255)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True

while running :
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        running = False

    pygame.draw.line(screen, BLANC, (10,20), (150,200), 2)
    pygame.display.update()

pygame.quit()
```

La ligne `pygame.draw.line(...)` permet d'afficher une ligne, la méthode `line` prend en paramètres :
- la surface sur laquelle nous allons dessiner (screen)  
- la couleur de la ligne au format `(r, g, b)` (tuple de 3 nombres compris entre 0 et 255), ici on a une ligne blanche avec (255, 255, 255)  
- coordonnées du point de départ : `tuple (x, y)`. Ici notre point de départ a pour coordonnées (10, 20)  
- coordonnées du point d'arrivée' : `tuple (x, y)`. Ici notre point d'arrivée a pour coordonnées (150, 200)  
- épaisseur de la ligne. Ici nous avons une épaisseur de 2 pixels

!!! danger "Attention"
    le point de coordonnées (0, 0) est en haut et à gauche de la fenêtre

La ligne `pygame.display.update()` indique à `Pygame` qu'il faut afficher tout ce qui doit être affiché (cette ligne est nécessaire à partir du moment où vous cherchez à dessiner quelque chose).


### Dessiner un cercle
Saisissez, analysez et testez ce code

```python linenums="1"
import pygame

pygame.init()

ROUGE = (255, 0, 0)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
while running :
 = True
while running :
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        while running :
 = False

    pygame.draw.circle(screen, ROUGE, (400,300), 30, 2)
    pygame.display.update()

pygame.quit()
```

La ligne `pygame.draw.circle(...)` permet d'afficher un cercle, la méthode `circle` prend en paramètres :
- la surface sur laquelle nous allons dessiner (`screen`)  
- la couleur de la ligne au format `(r, g, b)` (tuple de 3 nombres compris entre 0 et 255), ici on a un cercle rouge avec (255, 0, 0)  
- coordonnées du centre du cercle : `tuple (x,y)`. Ici le centre du cercle a pour coordonnées (400, 300)  
- rayon du cercle. Ici notre rayon est de 30 pixels  
- épaisseur de la ligne. Ici nous avons une épaisseur de 2 pixels

Il est possible de dessiner d'autres formes : rectangle, polygone, ellipse... Pour en savoir plus, n'hésitez pas à consulter la documentation de `Pygame`

## Animations

Nous allons déplacer notre cercle en modifiant ces coordonnées à chaque tour de boucle.

Saisissez, analysez et testez ce code

```python linenums="1"
import pygame

pygame.init()

ROUGE = (255, 0, 0)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True
posX = 50

while running :
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        running = False

    pygame.draw.circle(screen, ROUGE, (posX,300), 30, 2)
    posX = posX + 1
    pygame.display.update()

pygame.quit()
```

Comme vous pouvez le constater, les cercles précédents restent affichés. Il faut donc effacer l'image précédente avant de pouvoir en afficher une nouvelle.

Saisissez, analysez et testez ce code

```python linenums="1"
import pygame

pygame.init()

NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True
posX = 50

while running :
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    screen.fill(NOIR)
    pygame.draw.circle(screen, ROUGE, (posX,300), 30, 2)
    posX = posX + 1
    pygame.display.update()

pygame.quit()
```

la ligne `screen.fill(NOIR)` permet d'effacer l'écran avant de réafficher le cercle à une position différente. `fill` prend en paramètre un tuple qui permet de définir la couleur de fond de la surface (ici avec (0, 0, 0) nous avons du noir en arrière-plan).

Il est important de bien comprendre que même si vous n'avez pas d'animation à gérer, il est important d'effacer la surface avant d'afficher une nouvelle image. Nous utiliserons donc systématiquement le `fill`.

## Une balle qui rebondit
Saisissez, analysez et testez ce code

```python linenums="1"
import pygame

pygame.init()

NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True
posX = 50
vx = 1

while running :
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    screen.fill(NOIR)
    pygame.draw.circle(screen, ROUGE, (posX,300), 30, 2)
    
    if (posX > 770) or (posX < 30) :
        vx = -vx

    posX = posX + vx
    pygame.display.update()

pygame.quit()
```

## Limitation du nombre d'exécution de la boucle de jeu

Le système essaye d'exécuter le plus grand nombre de fois possible la "boucle de jeu". Sachant que plus votre microprocesseur est rapide et plus le nombre d'exécutions de la "boucle de jeu" par seconde sera grand, vous risquez d'avoir un jeu qui ne tournera pas du tout de la même façon sur 2 machines différentes. Pour éviter cet inconvénient, il est possible de limiter le nombre d'exécutions de la "boucle de jeu" par seconde .

Saisissez, analysez et testez ce code

```python linenums="1"
import pygame

pygame.init()

NOIR = (0, 0, 0)
ROUGE = (255, 0, 0)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True
posX = 50
vx = 1
clock = pygame.time.Clock()

while running :
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    clock.tick(60)
    screen.fill(NOIR)
    pygame.draw.circle(screen, ROUGE, (posX,300), 30, 2)

    if (posX > 770) or (posX < 30) :
        vx = -vx

    posX = posX + vx
    pygame.display.update()

pygame.quit()
```

Nous avons ajouté 2 lignes afin de contrôler le nombre d'exécutions de la "boucle de jeu" par seconde :
- `clock = pygame.time.Clock()` permet de définir un système d'horloge  
- `clock.tick(60)` permet de limiter le nombre d'exécutions de la "boucle de jeu" à 60 par seconde (nous aurons donc une fréquence d'affichage de 60 images par seconde)

Ce système de limitation est surtout important si vous avez à gérer des animations.

## Afficher une image

Commencez par télécharger une [image](IMG/pyg.png). Placez-là dans votre répertoire courant (là où vous avez placé vos programmes Python Pygame).

Saisissez, analysez et testez ce code

```python linenums="1"
import pygame

pygame.init()

NOIR = (0, 0, 0)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True
img = pygame.image.load("pyg.png")

while running :
  for event in pygame.event.get():
      if event.type == pygame.QUIT:
          running = False

  screen.fill(NOIR)
  screen.blit(img, (200, 200))
  pygame.display.update()

pygame.quit()
```

L'affichage de l'image se fait en 2 étapes :

1. création d'un objet de type image (`img` dans notre exemple) à l'aide de la méthode `load`. Cette méthode `load` prend un seul paramètre : l'url de l'image  
2. utilisation de la méthode `blit` pour afficher l'image (la méthode `blit` doit être utilisée dans la "boucle de jeu"). La méthode `blit` prend 2 paramètres : l'objet image à afficher (dans notre cas `img`) et un tuple qui correspond aux coordonnées du coin haut-gauche de l'image ((200,200) dans notre exemple)

## Ajouter une image de fond
Commencez par télécharger une [image](IMG/background.jpg). Placez-là dans votre répertoire courant (là où vous avez placé vos programmes Python Pygame).

La variable `fond` n'est qu'une référence à une Surface de `Pygame`, retournée par la fonction `load()`. Une Surface est une classe d'objets définie dans `Pygame` qui possède de nombreux attributs et méthodes. La méthode `convert()` des objets Surface sert à convertir l'image source au format utilisé par `Pygame`.

Le principe d'affichage est à connaître pour bien afficher ses images : `screen` est une surface vide, sur laquelle on va "coller", ou "empiler" les autres images. Le fond doit donc être empilé sur la surface vide de la fenêtre, grâce à la méthode `blit()`. Cette méthode prend une Surface en argument ainsi qu'un tuple représentant les coordonnées du coin supérieur gauche auquel sera collé la Surface argument par rapport à la Surface appelante.

Saisissez, analysez et testez ce code

```python linenums="1"
import pygame

pygame.init()

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True
img = pygame.image.load("pyg.png")

fond = pygame.image.load("background.jpg").convert()
screen.blit(fond, (0, 0))

while running :
  for event in pygame.event.get():
      if event.type == pygame.QUIT:
          running = False

  screen.blit(img, (200, 200))
  pygame.display.update()

pygame.quit()
```

??? bogue "mais pourquoi cela n'affiche rien ?"
    Parce que quand on `blit` une Surface, `pygame` calcule ce qu'il faut mais ne l'exécute pas réellement. Il faut forcer le rafraîchissement de l'écran pour y parvenir, par l'intermédiaire de la commande `pygame.display.update()`.

    Comme nous comptons bien faire bouger un personnage sur l'écran, et que les mouvements de celui-ci dépendront de la boucle d'évènements, autant mettre immédiatement cette commande en fin de boucle, pour que l'image soit systématiquement mise à jour. :

    ```python linenums="1"
    import pygame

    pygame.init()

    SCREEN_WIDTH = 800
    SCREEN_HEIGHT = 600

    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    running = True
    img = pygame.image.load("pyg.png")

    fond = pygame.image.load("background.jpg").convert()
    screen.blit(fond, (0,0))

    while running :
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
        
        screen.blit(img,(200,200))
        pygame.display.update()

    pygame.quit()
    ```



## Gestion de la souris
Comme déjà dit plus haut, il est possible de gérer les événements `clavier` et `souris` (utilisation du clavier et de la souris par l'utilisateur). Nous allons ici uniquement nous intéresser aux événements `souris`. Si vous avez besoin d'utiliser les événements `clavier` n'hésitez pas à consulter la documentation de pygame.

Il existe un événement `pygame.MOUSEBUTTONDOWN` qui correspond à un clic de souris. La méthode `get_pressed` renvoie un tuple constitué de 3 éléments. En l'absence de clic de souris, ce tuple est (0,0,0). En cas de clic sur le bouton gauche de la souris le tuple est (1,0,0). En cas de clic sur le bouton central le tuple est (0,1,0). En cas de clic sur le bouton droit le tuple est (0,0,1)

Saisissez, analysez et testez ce code. Cliquez avec votre souris (clic gauche et clic droit) dans la fenêtre pygame, observez attentivement la console.

```python linenums="1"
import pygame

pygame.init()

NOIR = (0, 0, 0)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True

while running :
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
          running = False

        if event.type == pygame.MOUSEBUTTONDOWN :
            if pygame.mouse.get_pressed() == (1,0,0) :
                print ("clic bouton gauche")

            if pygame.mouse.get_pressed() == (0,0,1) :
                print ("clic bouton droit")
    
    screen.fill(NOIR)
    pygame.display.update()
pygame.quit()
```

Il est aussi possible de récupérer les coordonnées du pointeur de la souris au moment du clic à l'aide de la méthode `mouse.get_pos`. Cette méthode renvoie un tuple (x,y) donnant les coordonnées du pointeur de la souris au moment du clic.

Saisissez, analysez et testez ce code. Cliquez avec votre souris (clic gauche) dans la fenêtre pygame, observez attentivement la console

```python linenums="1"
import pygame

pygame.init()

NOIR = (0, 0, 0)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True

while running :
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.MOUSEBUTTONDOWN :
            if pygame.mouse.get_pressed() == (1,0,0) :
                pos = pygame.mouse.get_pos()
                print(pos)

    screen.fill(NOIR)
    pygame.display.update()
pygame.quit()
```

## Gestion du clavier
Saisissez, analysez et testez ce code, observez la console lorsque vous appuyez sur les touches `Entrée`, `Espace` et `A`.

```python linenums="1"
import pygame

pygame.init()

NOIR = (0, 0, 0)

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
running = True

while running :
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
          running = False

        if event.type == pygame.KEYDOWN :
            if event.key == pygame.K_SPACE :
                print ("vous avez appuyé sur la touche espace")
            elif event.key == pygame.K_a :
                print ("vous avez appuyé sur la touche A")
            elif event.key == pygame.K_RETURN :
                print ("vous avez appuyé sur la touche Entrée")
            else :
                print ("vous avez appuyé sur une touche")

    screen.fill(NOIR)
    pygame.display.update()
pygame.quit()
```

Vous trouverez la liste complète des touches dans la documentation officielle de pygame.


<!-- ## Ajouter un sprite
On ajoute hors de la boucle l'instruction `perso = pygame.image.load("perso.png").convert()`, suivie de `screen.blit(perso, (270,380))` pour le placer aux coordonnées (270, 380) (mais après avoir collé le fond).

!!! info "Transparence et canal alpha"
    === "Canal Alpha"
        Le résultat est peu probant. En effet nous voyons un cadre noir autour sprite du personnage. Il va donc falloir ajouter de la transparence à cette image.

        Cette possibilité est offerte par le format `png`, qui possède un format de couleur basé sur la système `RGB + canal Alpha`. Un pixel est donc représenté par 4 octets :
        
        - Les trois premiers pour les canaux RGB, chacun étant donc représenté par un nombre entre 0 et 255 ( sommairement 0 représentant le canal éteint, et 255 le canal allumé au maximum) ;

        - Le dernier octet pour le canal Alpha, qui va représenter le niveau de transparence du pixel. Ainsi un pixel possédant un canal Alpha à 0 sera totalement transparent, alors qu'avec une valeur de 255, il sera totalement opaque.

        Pygame est bien entendu capable de gérer cette transparence, il suffit d'utiliser la méthode `convert_alpha()` à la place de la méthode `convert()`.
    === "et les autres formats ?"
        Le format `png` est à privilégier pour l'utilisation de sprites. Cependant il est aussi possible de forcer une couleur d'une image de format quelconque à devenir transparente, grâce à la méthode `set_colorkey()` utilisée comme dans la ligne suivante :
        ```python
        BLANC = (255,255,255)
        image.set_colorkey(BLANC)
        ```

        Ici on a rendu la couleur blanche (triplet RGB) transparente pour l'objet image. -->

