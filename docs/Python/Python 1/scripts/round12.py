# 22/7, une fraction proche de pi
print("Un arrondi à 5 chiffres de 22/7 est", round(22/7, 5))

x = 123456789.0123456789
# x est un grand nombre ; trop précis
print("À 1000 près, x est égal à", round(x, -3))