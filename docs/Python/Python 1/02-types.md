# 🦎 Types

## 🧮 Entier

### Quatre premières opérations

> On évoquera la division dans la section suivante.

!!! tip "Avec Python, pour deux entiers"

    | Opération      |Opérateur|
    |:---------------|:-------:|
    | Addition       | `+`  |
    | Soustraction   | `-`  |
    | Multiplication | `*`  |
    | Puissance      | `**` |

!!! savoir "À savoir"
    - Les priorités usuelles sont respectées.
    - Les multiplications doivent être **explicites** :
        - On peut écrire `3*n + 1` pour $3n + 1$,
        - **mais pas** `3n + 1` qui provoquera une erreur.
    - On peut mettre des parenthèses `( )` pour forcer les priorités,
        - **mais pas** de crochets `[ ]` ou accolades `{ }` qui ont un autre rôle.
    - L'opérateur pour la puissance est `**`
    - Le calcul sur les entiers est en précision arbitraire :
        - La seule limite étant la mémoire de l'ordinateur.
        - On peut **travailler** avec des entiers qui ont des millions de chiffres,
        - mais **pas afficher** ceux qui sont trop grands.


### Exemples

```pycon
>>> 43 * 47
2021
>>> (45 - 2) * (45 + 2)
2021
>>> 23**2 + 197**2  # 23 au carré, plus 197 au carré
39338
>>> 97**2 + 173**2
39338
>>> 107**2 + 167**2
39338
>>> 113**2 + 163**2
39338
```

!!! cite "Humour _geek_"
    ![](IMG/xkcd-859.png){ title="Brains aside, I wonder how many poorly-written xkcd.com-parsing scripts will break on this title (or ;;&quot;&#39;&#39;{&lt;&lt;[&#39; this mouseover text.&quot;" alt="(" align=left width=350 }

    Source : [XKCD](https://xkcd.com/859/){ target=_blank }

### En savoir plus

- Les entiers sont stockés en mémoire sous forme binaire, il n'y a que deux chiffres `0` et `1` ; les calculs sont plus pratiques pour le processeur. Par exemple $42$ est stocké avec le code binaire `101010`.

!!! tip "Entrer de grands nombres"
    En bref :

    - `100_000` est plus lisible que `100000`.
    - `100 000` provoque une erreur, il manque le tiret-bas.

    Avec une version récente de Python, on peut utiliser le
     caractère `_` (tiret-bas) comme séparateur de milliers, millions, etc.

    - On recommande de le faire pour les nombres à partir de 5 chiffres.

    - Avec d'autres langages de programmation ou une ancienne version
     de Python, on peut écrire `60 * 1000 * 1000` par exemple.

## ➗ Division entière et modulo

### Les opérateurs

!!! tip "Avec Python, pour deux entiers"

    | Opération       |Opérateur|
    |:----------------|:----:|
    | Quotient entier | `//` |
    | reste entier    | `%`  |

### Exemples

```pycon
>>> 1984 // 100
19
>> 1984 % 100
84
```

!!! warning  "Ne pas oublier de doubler le caractère `/`"
    Sinon, l'opérateur `/` renvoie un nombre en virgule flottante (`flottant`).

    ```python
    >>> 1984 / 100
    19.84
    ```

## ♾️ Flottants

!!! warning "Les flottants"
    Les flottants [^1] (_**float**ing point numbers_) s'utilisent comme les nombres décimaux, **mais il y aura des précautions à prendre**.

    [^1]: :fontawesome-brands-wikipedia-w: Les flottants ; la norme [IEEE_754](https://fr.wikipedia.org/wiki/IEEE_754){ target=_blank }

### Les choses simples avec les flottants

!!! savoir "À savoir"
    - Le point `.` est le séparateur décimal.
    - Ces nombres sont stockés en binaire (et non en décimal),
        - avec une précision souvent meilleure qu'avec une calculatrice,
        - mais pas arbitraire non plus.
    - On peut entrer directement un nombre en écriture scientifique en utilisant la notation [^2] `e`
        - exemple : `-1.602e-19` pour $-1,602 \times 10^{-19}$, la charge en coulomb d'un électron.

    [^2]: :fontawesome-brands-wikipedia-w: La [notation e (_en_)](https://en.wikipedia.org/wiki/Scientific_notation#E-notation){ target=_blank }

!!! danger "Le nombre stocké est souvent différent du nombre entré"
    - Le nombre stocké sera l'approximation binaire du nombre décimal entré et sera souvent différent !
    - :warning: Le nombre en binaire sera encore un nombre décimal... mais pas forcément le même.

!!! example "Exemples simples expliqués"

    ```python
    In [1]: from math import pi, sqrt  # Explication (1)

    In [2]: pi / 2  # Explication (2)
    Out[2]: 1.5707963267948966

    In [3]: 1.2**1000  # Explication (3)
    Out[3]: 1.5179100891722457e+79

    In [4]: 7.3 + 2  # Explication (4)
    Out[4]: 9.3

    In [5]: 21 / 3  # Explication (5)
    Out[5]: 7.0

    In [6]: 18 / 6.02e23  # Explication (6)
    Out[6]: 2.990033222591362e-23

    In [7]: sqrt(12.3)  # Explication (7)
    Out[7]: 3.5071355833500366
    ```

    1. Depuis (_from_) le module `math`, on importe :
        - une bonne approximation de π par un flottant,
        - une fonction racine carrée (_**sq**uare **r**oo**t**_) qui renvoie un flottant.
    2. Une approximation de π/2 donnée avec une quinzaine de chiffres décimaux significatifs.
        - La division entre flottants s'obtient avec l'opérateur `/`
    3. Un calcul d'une puissance d'un flottant.
        - Le résultat est donné en écriture scientifique, environ 1,5179×10⁷⁹
    4. On peut mélanger un entier et un flottant dans une opération.
        - **Détail important** : l'entier sera d'abord converti automatiquement en flottant avant le calcul.
    5. Si on utilise l'opérateur `/`,
        - les opérandes entiers sont automatiquement convertis en flottant avant le calcul ;
        - le résultat sera un flottant, même si la division entière a un reste nul.
    6. L'avant-dernier exemple calcule le volume moyen d'une molécule d'eau[^3] en ml, soit environ 30 Å^3^.
    7. On calcule la racine carrée d'un flottant.

    [^3]: :fontawesome-brands-wikipedia-w: La [constante d'Avogadro](https://en.wikipedia.org/wiki/Avogadro_constant){ target=_blank }


## Points délicats

!!! warning "Attention"
    On retrouve comme sur de nombreuses calculatrices (et c'est normal) les points suivants :

    - Il existe des limites aux nombres flottants, avec
        - un plus petit flottant strictement positif,
        - un plus grand flottant positif,
        - et de même avec les négatifs.
    - Le nombre affiché ou entré n'est souvent pas égal au nombre représenté en machine.
    - Pour simplifier :
        - une quinzaine de chiffres significatifs, et
        - des exposants de la puissance de 10 entre -300 et +300, environ.

!!! example "Exemples expliqués"

    ```python
    In [1]: 0.5**1000  # cette opération renvoie (1)
    Out[1]: 9.332636185032189e-302

    In [2]: 0.5**1100  # cette opération renvoie (2)
    Out[2]: 0.0

    In [3]: 2.0**1000  # cette opération renvoie (3)
    Out[3]: 1.0715086071862673e+301

    In [4]: 2.0**1100  # cette opération renvoie (4)
    Traceback (most recent call last):

      File "<stdin>", line 1, in <module>

    OverflowError: (34, 'Numerical result out of range')
    ```

    1. un résultat très petit, qui se rapproche du plus petit flottant strictement positif.
    2. un résultat, tellement petit, qu'il est arrondi à exactement zéro, en flottant.
    3. un résultat très grand, écrit en écriture scientifique.
    4. une erreur, le résultat étant trop grand.
      

    :warning: Notons que `2**1100` ne provoquerait **pas d'erreur** ; c'est un **entier** qui pourrait être bien plus grand encore sans perdre de précision, tant qu'il y a de la mémoire disponible.

### Stockage interne, souvent une approximation

!!! danger "Exemple à méditer"

    ```python
    In [1]: 0.1 + 0.2 == 0.3  # C'est choquant, mais (1)
    Out[1]: False
    ```

    1. **c'est Faux**

    Explications :

    - $0.1$ est approché en machine par un nombre qui n'est pas exactement égal à $0.1$[^5], mais par un nombre écrit en binaire, au plus proche. De même pour $0.2$ et $0.3$.
    - Le test d'égalité est réalisé sur les nombres binaires, pas sur les nombres affichés en décimal !
    - Une calculatrice a normalement le **même comportement**, sauf bug.

    [^5]: L'approximation de $0.1$ est stockée avec la valeur $\dfrac{3602879701896397}{2^{55}} =\dfrac{3602879701896397}{36028797018963968} = 0,\!1000000000000000055511151231257827021181583404541015625$ qui est lui aussi un décimal, mais surtout un quotient d'entier par une puissance de deux.

!!! savoir "Conclusion, à retenir"
    1. On ne doit **jamais** faire de test d'égalité entre flottants !
        - Par exemple, pour savoir si un triangle à côtés flottants $(a < b < c)$ est rectangle,
            - **on ne fera pas** le test `a*a + b*b == c*c`.
    2. Il faut apprendre à faire des arrondis à la précision que l'on souhaite.
        - Nous allons donc découvrir, entre autres, les fonctions natives `round` et `abs`.

### Exercice

!!! faq "Triangle rectangle"
    1. En utilisant Python, vérifier si le triangle $ABC$ est rectangle sachant que
      
        - $AB = 6.5~\text{cm}$
        - $BC = 7.2~\text{cm}$
        - $CA = 9.7~\text{cm}$
      
    2. Et si les longueurs étaient exprimées en millimètre ?

??? done "Réponse"
    1. Oui, ce triangle est rectangle en $B$.
        
        $CA$ est le plus grand des côtés, et on a :

        === "✅ Bonne méthode"
            ```pycon
            >>> AB = 6.5
            >>> BC = 7.2
            >>> CA = 9.7
            >>> abs(AB**2 + BC**2 - CA**2) < 10**-6
            True
            ```
        === "❌ Mauvaise méthode"
            ```pycon
            >>> AB = 6.5
            >>> BC = 7.2
            >>> CA = 9.7
            >>> AB**2 + BC**2 == CA**2
            False
            ```
      
    2. Avec les nombres entiers, c'est plus simple
      
        ```pycon
        >>> AB = 65
        >>> BC = 72
        >>> CA = 97
        >>> AB**2 + BC**2 == CA**2
        True
        ```


# 🦎 Bilan sur les `types` 

## Les types numériques

`bool, int, float, complex` sont des types numériques et aussi des fonctions de conversion pour passer quand c'est possible d'un type à un autre (booléen, entier, flottant ou complexe).


## Les types de données structurées

`tuple, str, list, set, dict` sont des types de données, et aussi des fonctions de conversion.

- `tuple` pour un couple, un triplet, ou plus généralement un [n-uplet](https://fr.wikipedia.org/wiki/N-uplet).
- `str` pour une chaine de caractère.
- `list` pour un tableau ou une liste ordonnée.
- `set` pour un ensemble ; non ordonné, sans doublon.
- `dict` pour un dictionnaire ; un ensemble de clés et la valeur associée à chaque clé.

## Remarques techniques

!!! danger "Section délicate"
    Cette section est réservée à ceux qui veulent en savoir davantage.

### Obtenir le type d'un objet

!!! example "Exemples expliqués"
    ```python
    In [1]: type(1)
    Out[1]: int

    In [2]: type(1.)
    Out[2]: float

    In [3]: 1. is 1
    Out[3]: False

    In [4]: 1. == 1
    Out[4]: True
    ```

    !!! info "Remarques"
        1. `1` est de type entier, (_**int**eger_)
        2. `1.` ou bien `1.0` est de type flottant, (_**float**ing point number_)
        3. Ce **ne sont pas** les mêmes objets en interne pour Python. `#!python is` répond alors `False` pour faux.
        4. À la comparaison, il se passe un phénomène de changement de type. Pour être comparé à un flottant, un entier est automatiquement converti en flottant. Et là, la comparaison s'avère égale, donc le test d'égalité renvoie `#!python True` (pour vrai). Nous avons aussi évoqué ce phénomène pour une opération entre un flottant et un entier.

!!! danger "Cas des flottants"
    ```python
    In [22]: 0.1 + 0.0045
    Out[22]: 0.1045

    In [23]: (5.4 + 2.7)
    Out[23]: 8.100000000000001

    In [24]: (5,4 + 2,7)
    Out[24]: (5, 6, 7)
    ```

    - Le premier exemple montre ce qu'il se passe fréquemment : l'affichage décimal de la somme de deux flottants (issus de décimaux) est égale à la somme des décimaux d'origine. *Cette phrase était complexe ; reformulons*. Dit autrement : l'addition de deux décimaux, provoque en machine l'addition de deux nombres en binaires qui ne sont pas égaux aux décimaux, mais la somme calculée (qui sera un nombre en binaire) peut s'écrire souvent en décimal comme la somme des décimaux d'origine.
    - Dans le second exemple, on constate que ce n'est pas une généralité.
    - Dans le troisième exemple, Python a affiché les trois éléments d'un *tuple*, le second étant 4+2, égal à 6. Nous verrons les tuples juste après. Ici, il n'y a pas d'erreur à l'exécution, on parlera éventuellement d'erreur sémantique.
