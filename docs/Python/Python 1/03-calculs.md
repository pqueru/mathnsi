# ➕ Calculs simples

1. Les variables peuvent stocker des résultats issus de calculs.
2. On peut afficher le contenu d'une variable.

Commençons par des calculs simples.

!!! tip "Avec Python, pour deux entiers"

    | Opération      |Opérateur|
    |:---------------|:-------:|
    | Addition       | `+`  |
    | Soustraction   | `-`  |
    | Multiplication | `*`  |


# Modifier une variable

Anticiper, étudier et expliquer les différentes sorties.

=== "Cas n°1"

    ```python
    hauteur = 4
    largeur = 5
    aire = largeur * hauteur
    print(aire)
    largeur = 7
    aire = largeur * hauteur
    print(aire)
    ```

    ??? done "Résultat"
        
        ```output
        20
        28
        ```

    ??? example "Explication"
        1. L'`aire` est calculée avec $5 × 4$, puis affichée. $20$
        2. L'`aire` est à nouveau calculée avec $7 × 4$, puis affichée. $28$.

        La `hauteur` n'a pas été modifiée, mais la `largeur` oui.

        La variable `aire` a aussi été modifiée.


=== "Cas n°2"

    ```python
    hauteur = 4
    largeur = 5
    aire = largeur * hauteur
    print(aire)
    hauteur = hauteur + 3
    print(aire)
    ```

    ??? done "Résultat"
        
        ```output
        20
        20
        ```

    ??? example "Explication"
        1. L'`aire` est calculée **une seule fois**, mais affichée deux fois avec la même valeur.
        2. Oui, la `hauteur` a été modifiée de $4$ à $7$.

        ```python
        hauteur = hauteur + 3
        ```

        Ce code se lit : « la nouvelle valeur de `hauteur` est égale à l'ancienne valeur de `hauteur` plus $3$ ».



=== "Cas n°3"

    ```python
    hauteur = 4
    largeur = 5
    aire = largeur * hauteur
    print(aire)
    hauteur = hauteur + 3
    largeur = largeur - 2
    aire = largeur * hauteur
    print(aire)
    ```

    ??? done "Résultat"
        
        ```output
        20
        21
        ```

    ??? example "Explication"
        L'`aire` est calculée puis affichée deux fois.

        1. Première fois avec $4 × 5$ qui donne $20$.
        2. Deuxième fois avec $7 × 3$ qui donne $21$.

        En effet,

        1. `hauteur` a augmenté de $3$. Passe de $4$ à $7$.
        2. `largeur` a diminué de $2$. Passe de $5$ à $3$.