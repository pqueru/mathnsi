# ❓ Test et condition

## Comparaisons d'entiers


!!! tip "Comparaisons avec Python"
    | Comparaison               |Opérateur|
    |:--------------------------|:----:|
    | Strictement inférieur à   | `<`  |
    | Inférieur ou égal à       | `<=` |
    | Égal à                    | `==` |
    | Différent de              | `!=` |
    | Supérieur ou égal à       | `>=` |
    | Strictement supérieur à   | `>`  |

    Ces opérateurs entre deux entiers renvoient un **booléen** :

    - `True`, pour Vrai
    - `False`, pour Faux

!!! example "Exemples"

    ```pycon
    >>> 153 > 98
    True
    >>> (6 - 6) <= 0
    True
    >>> 15 != 10 + 5
    False
    ```

## Test simple

=== "Python"

    ```python
    if condition:
        action_1()
        action_2()
    action_3()
    ```

=== "pseudo code"

    ```
    SI condition EST_VRAIE ALORS
        action_1
        action_2
    action_3
    ```

=== "Exemple 1"

    ```email
    19
    ```

    ```python
    age = int(input())

    if age >= 18:
        print("Vous pouvez voter")
        print("Vous devriez y réfléchir sérieusement")
    print("Vous observerez le résultat")
    ```

    ```output
    Vous pouvez voter
    Vous devriez y réfléchir sérieusement
    Vous observerez le résultat
    ```

=== "Exemple 2"

    ```email
    17
    ```

    ```python
    age = int(input())

    if age >= 18:
        print("Vous pouvez voter")
        print("Vous devriez y réfléchir sérieusement")
    print("Vous observerez le résultat")
    ```

    ```ouput
    Vous observerez le résultat
    ```

- Les actions 1 et 2 ne sont effectuées que **si** la condition est Vraie.
- L'action 3 sera faite ensuite dans tous las cas (sauf si le programme s'arrête entre temps).

!!! abstract "La syntaxe"
    - Il faut commencer par `#!python if`, c'est un mot clé.
    - On écrit ensuite la condition, résultat d'un test.
    - On finit par `:` comme pour une structure de répétition avec `#!python for`
    - On écrit les instructions concernées dans un bloc décalé. On dit **indenté**. On utilise la touche ++tab++, qui devrait normalement générer 4 espaces.
    - On désindente pour sortir de la structure.

## Structure conditionnelle

Avec Python, on a les constructions suivantes possibles :

=== "Simple"

    ```python
    if condition:
        action()
    ```

    Si `condition` est évaluée à `#!python True`

    - alors `action()` est exécutée.

=== "Classique"

    ```python
    if condition:
        action()
    else:
        autre_action()
    ```

    Si `condition` est évaluée à `#!python True`

    - alors `action()` est exécutée,
    - sinon, `autre_action()` est exécutée.

=== "Élaborée"

    ```python
    if condition_1:
        action_1()
    elif condition_2:
        action_2()
    elif condition_3:
        action_3()
    else:
        action_autres_cas()
    ```

    - `#!python elif` est la contraction de `#!python else if`, pour « sinon si ».
    - Il peut y avoir autant de blocs `#!python elif` que l'on souhaite.
    - On peut terminer, par un bloc `#!python else` pour les cas non traités.

    Cette construction est utilisée classiquement pour traiter différemment
    une liste de cas.

## Exercice

!!! note "Que dire des scripts suivants ?"

    - Sont-ils corrects ?
    - Sont-ils équivalents ?
    - Sont-ils bien écrits ?

    === "Script A"

        ```python
        if condition_1:
            ####################
            #                 ##
            action_1()        ##
            #                 ##
            ####################
        else:
            if condition_2:
                ####################
                #                 ##
                action_2()        ##
                #                 ##
                ####################
            else:
                if condition_3:
                    ####################
                    #                 ##
                    action_3()        ##
                    #                 ##
                    ####################
                else:
                    ####################
                    #                 ##
                    action_0()        ##
                    #                 ##
                    ####################

        ```

    === "Script B"

        ```python
        if condition_1:
            ####################
            #                 ##
            action_1()        ##
            #                 ##
            ####################
        elif condition_2:
            ####################
            #                 ##
            action_2()        ##
            #                 ##
            ####################
        elif condition_3:
            ####################
            #                 ##
            action_3()        ##
            #                 ##
            ####################
        else:
            ####################
            #                 ##
            action_0()        ##
            #                 ##
            ####################
        ```

    === "Script C"

        ```python
        if condition_1:
            ####################
            #                 ##
            action_1()        ##
            #                 ##
            ####################
        elif condition_2:
                ####################
                #                 ##
                action_2()        ##
                #                 ##
                ####################
            elif condition_3:
                    ####################
                    #                 ##
                    action_3()        ##
                    #                 ##
                    ####################
                else:
                    ####################
                    #                 ##
                    action_0()        ##
                    #                 ##
                    ####################
        ```

    === "Script D"

        ```python
        if condition_1:
            ####################
            #                 ##
            action_1()        ##
            #                 ##
            ####################
        elif condition_2:
                ####################
                #                 ##
                action_2()        ##
                #                 ##
                ####################
        elif condition_3:
                    ####################
                    #                 ##
                    action_3()        ##
                    #                 ##
                    ####################
        else:
                        ####################
                        #                 ##
                        action_0()        ##
                        #                 ##
                        ####################
        ```

??? done "Solution"
    === "Sont-ils corrects ?"
        Le script C est incorrect, `#!python elif condition_3:` est mal indenté,
         le dernier `#!python else` également.

        Un `#!python elif` doit être indenté au même niveau que le `#!python elif`
         précédent ou `#!python if` précédent.
    
    === "Sont-ils équivalents ?"
        Les scripts A, B et D sont équivalents ; **oui**, techniquement.

    === "Sont-ils bien écrits ?"
        Le D est mal écrit, on a le droit d'indenter chaque bloc autant qu'on veut,
         mais, ici, c'est mal choisi.

        Le A est maladroit. On préfère utiliser `#!python elif` dans ce cas là.

        Le B, voilà la **bonne méthode**.