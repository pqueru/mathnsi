# 🪄 Création de Fonction

Observons d'abord quelques utilisations de fonctions créées.

## Quelques exemples

=== "Polynôme"

    ```python
    def f(n):
        return n * (n+1) // 2
    ```

    ```pycon
    >>> f(5)
    15
    ```

=== "Géométrie"

    ```python
    def périmètre_rectangle(base, hauteur):
        return 2*(base + hauteur)
    ```

    ```pycon
    >>> périmètre_rectangle(base=7, hauteur=3)
    20
    ```

!!! cite "Premières définitions de fonctions"
    1. On commence par le mot clé `#!python def`
    2. On donne un **nom de variable** à la fonction
    3. On donne les paramètres : zéro, un ou plusieurs ; entre parenthèses, séparés par des virgules
    4. On termine cette ligne par `:`
    5. On donne le corps de la fonction (sur la même ligne) ou (mieux)
     sur plusieurs lignes ; dans ce cas, c'est un bloc indenté.
    6. Il peut y avoir un ou plusieurs `#!python return`.
        - Si la fonction arrive à la fin du bloc un `#!python return None` est automatiquement ajouté.
        - Une **fonction renvoie toujours** une valeur, avec `#!python return`.

```python
def nom_explicite_de_la_fonction(parametre1, parametre2, ...):
    #####################################
    #                                   #
    corps dela fonction                 #
    #                                   #
                    return ...          #
            return ...                  #
    #####################################
    return ...
```


!!! tip "Pour utiliser une fonction"
    **Dans un premier temps**, on conseille

    1. D'écrire la fonction dans un script, avec [Thonny](https://thonny.org/){ target=_blank }, [Colaboratory](https://colab.research.google.com/){ target=_blank }, [Basthon (console ou carnet)](https://basthon.fr/){ target=_blank } ou [NumWorks](https://www.numworks.com/simulator/){ target=_blank }, ou directement ici avec Pyodide.
    2. :fontawesome-solid-gears: Exécuter ce script.
    3. Appeler la fonction dans la console.

!!! abstract "Bonnes pratiques sur les noms de variables"

    1. Contrairement aux mathématiques, avec Python :
        - On recommande donc d'utiliser des noms de variables à plusieurs lettres, qui donnent du sens, dès que possible.
    2. Les accents sont tolérés pour du code en français, à destination du public français.
        - Pour du code international, l'anglais (sans accent donc) est de rigueur. De même que pour d'autres langages de programmation. Il vaut mieux éviter les accents.
    3. On peut utiliser les noms des paramètres dans l'appel de
     la fonction,
        - comme avec `#!python périmètre_rectangle(hauteur=3, base=7)`.
        - C'est une bonne pratique.
        - Cela permet de changer l'ordre des paramètres, de mieux comprendre l'appel.
        - Pas d'espace autour du signe `=` dans ce cas.