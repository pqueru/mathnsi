# Chapitre 7 : Variables aléatoires

## Simulation d'une partie

Compléter la fonction suivante.

{{ IDE('./partie') }}

## Gain moyen

En vous servant de la fonction `partie`

{{ IDE('./gain_moyen') }}