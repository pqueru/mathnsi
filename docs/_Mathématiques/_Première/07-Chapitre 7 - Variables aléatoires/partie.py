import random

def partie():
    """
    Simulation d'une partie : 
    - on mise $3.
    - on lance un dé à 6 faces.
    - si on obtient 2, 4 ou 6, on gagne le double du numéro affiché par le dé.
    - si on obtient 1 ou 3, on gagne $1.
    - si on obtient 5, on ne gagne rien.
    """

    pass