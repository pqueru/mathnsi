#--- HDR ---#
import random

def partie():
    """
    Simulation d'une partie : 
    - on mise $3.
    - on lance un dé à 6 faces.
    - si on obtient 2, 4 ou 6, on gagne le double du numéro affiché par le dé.
    - si on obtient 1 ou 3, on gagne $1.
    - si on obtient 5, on ne gagne rien.
    """
    
    de = random.randint(1, 6)
    
    if de % 2 == 0:
        return 2 * de - 3

    elif (de == 1) or (de == 3):
        return -2
    
    else:
        return -3
#--- HDR ---#


def gain_moyen(n):
    """
    Calcul du gain moyen obtenu après n parties
    """
    
    pass