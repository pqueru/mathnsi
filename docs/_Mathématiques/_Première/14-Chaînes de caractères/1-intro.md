# Chaînes de caractères

## Longueur

On peut calculer la longueur d'une chaîne de caractères à l'aide de la fonction `len` :
```python
>>> len('bonjour')
7
```

On se propose de réecrire cette fonction.

Écrire une fonction `longueur` qui prend un paramètre une chaîne de caractères `mot` et qui renvoie sa longueur.

Votre fonction ne devra pas utiliser la fontion `len`.

{{ IDE('/1ere/Chaines/longueur', MAX = 10, SANS = 'len') }}

## Nombre d'occurences d'un caractère

On peut calculer le nombre d'occurences d'un caratère d'une chaîne de caractères à l'aide de la fonction `count` :
```python
>>> 'bonjour'.count('o')
2
```

Écrire une fonction `compter` qui prend en paramètre une `lettre` et un `mot` et qui renvoie le nombre de caractère `lettre` que l'on trouve dans la chaîne de caractères `mot` ; sans utiliser la méthode count().

Ainsi `compter('o', "bonjour")` renvoie 2 car il y a 2 'o' dans 'bonjour'.

{{ IDE('/1ere/Chaines/compter', MAX = 10, SANS = 'count') }}
