# Chapitre 3 : Trigonométrie

Pour utiliser les fonctions trigonométriques et $\pi$, il faut les importer du module `maths` : 
```python
>>> from math import sin, cos, tan, pi
>>> print(pi)
3.141592653589793
>>> print(pi / 2)
1.0
```