def suivant(u):
    if u % 2 == 0:
        return u // 2
    else:
        return 3*u + 1
    
def liste_syracuse(n):
    L = []
    tps = 0
    while n != 1:
        L.append(n)
        n = suivant(n)
        tps = tps + 1

    L.append(n)
    return (L, tps)