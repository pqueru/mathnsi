def calcul_terme(n):
    u = 1
    for i in range(n):
        u = (u - 1) / (u - 2)
    return u