assert ma_suite_table(10) == [1, 1, 3, 6, 10, 15, 21, 28, 36, 45]
assert ma_suite_table(0) == []
assert ma_suite_table(30) == [1, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120, 136, 153, 171, 190, 210, 231, 253, 276, 300, 325, 351, 378, 406, 435]