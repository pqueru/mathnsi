def ma_suite(n):
    i = 1
    u = 1
    while i < n:
        i = i + 1
        u = u + i
    return u

n = 10

t = [ma_suite(i) for i in range(n)]
