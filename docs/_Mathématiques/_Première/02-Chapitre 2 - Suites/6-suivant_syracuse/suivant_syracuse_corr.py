def suivant_syracuse(u):
    if u % 2 == 0:
        return u // 2
    else:
        return 3*u + 1