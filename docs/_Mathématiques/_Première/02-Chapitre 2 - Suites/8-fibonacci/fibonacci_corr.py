def fibonacci(n):
    x = 0
    y = 1
    for i in range(n-1):
        z = y + x
        x = y
        y = z
    return z