from random import *
import matplotlib.pyplot as plt
import numpy as np

def fonction(x):
    return x**2

def calculTirages(n):
    plt.figure(1)
    xmin, xmax = 0, 1
    ymin, ymax = 0, 1
    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.plot([xmin, xmax], [ymin, ymin], 'k-', color = "purple")
    plt.plot([xmin, xmin], [ymin, ymax], 'k-', color = "purple")
    plt.plot([xmin, xmax], [ymax, ymax], 'k-', color = "purple")
    plt.plot([xmax, xmax], [ymin, ymax], 'k-', color = "purple")

    plt.axis("equal")
    
    # Dessin de la courbe
    x = np.linspace(0, 1, 100)
    plt.plot(x, fonction(x))

    nbInside = 0
    
    for i in range(n):        
        x = random()
        y = random()
        plt.plot([x], [y], marker = '+', color = "blue")
        
        if isInside(x, y):
            nbInside += 1
    
    return nbInside / n

def isInside(x, y):
    return y <= fonction(x)

calculTirages(100)