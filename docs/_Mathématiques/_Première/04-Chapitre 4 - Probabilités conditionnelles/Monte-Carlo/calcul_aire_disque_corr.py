from random import random

def calcul_aire_disque(taille_echantillon):
    n = 0
    
    for i in range(taille_echantillon):
        x = random()
        y = random()

        if (x-.5)**2 + (y-.5)**2 <= .25:
            n = n + 1

    tmp = n / taille_echantillon
    return 4 * tmp