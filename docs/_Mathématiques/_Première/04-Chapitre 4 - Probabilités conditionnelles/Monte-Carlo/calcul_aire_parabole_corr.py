from random import random

def calcul_aire_parabole(taille_echantillon):
    n = 0
    
    for i in range(taille_echantillon):
        x = random()
        y = random()

        if y <= x**2:
            n = n + 1

    A = n / taille_echantillon
    return A