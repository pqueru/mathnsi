from math import sqrt

def discriminant(a, b, c):
    return b**2 - 4*a*c

def racines(a, b, c):
    delta = discriminant(a, b, c)
    if delta < 0:
        return []
    elif delta == 0:
        return [-b / (2*a)]
    else:
        return [(-b - sqrt(delta)) / (2*a), (-b + sqrt(delta)) / (2*a)]