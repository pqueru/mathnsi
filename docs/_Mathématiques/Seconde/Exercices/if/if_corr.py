def nouveau_prix(prix):
    """
    Renvoie le prix réduit de 5% si prix est supérieur ou égal à 100
    """
    if prix >= 100 :
        prix = prix * 0.95
        
    return prix