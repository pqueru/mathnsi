assert multiple(8, 2) == True
assert multiple(45, 3) == True
assert multiple(21, 7) == True
assert multiple(8, 3) == False
assert multiple(18, 5) == False