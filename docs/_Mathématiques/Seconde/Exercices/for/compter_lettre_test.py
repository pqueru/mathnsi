assert compter_lettre("e", "Ceci est un exemple") == 5
assert compter_lettre("t", "Albert Einstein") == 2
assert compter_lettre("e", "Vive les maths !") == 2
assert compter_lettre("l", "Vive les maths !") == 1