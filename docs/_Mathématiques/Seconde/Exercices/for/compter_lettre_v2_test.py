assert compter_lettre_v2("e", "Ceci est un exemple") == 5
assert compter_lettre_v2("t", "Albert Einstein") == 2
assert compter_lettre_v2("e", "Vive les maths !") == 2
assert compter_lettre_v2("l", "Vive les maths !") == 1