# Chapitre 10 : Arihtmétique

## Multiple
Écrire une fonction `est_multiple` qui prend en paramètres deux entiers $a$ et $b$ et qui renvoie `True` si $a$ est un multiple de $b$, `False` sinon.

{{ IDE('../Exercices/est_multiple/est_multiple', MAX = 5, SANS = 'max,min') }}