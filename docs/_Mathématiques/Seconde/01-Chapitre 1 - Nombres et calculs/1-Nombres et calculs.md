# Chapitre 1 : Nombres et calculs

## Nombre premier
Écrire une fonction `is_prime` qui prend en paramètre un nombre entier `n` et qui renvoie `True` si `n` est premier et `False` sinon.

{{ IDE('../Exercices/is_prime/is_prime', MAX = 5, SANS = 'max,min') }}
