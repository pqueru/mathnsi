# Chapitre 8 : Fonctions affines

## Exercice 1
Écrire une fontion `f` qui prend en paramètre un nombre $x$ et qui renvoie l'image de $x$ par la fonction $f ~:~  x \mapsto 3x - 5$.

{{ IDE('../Exercices/exo_f/f', MAX = 5, SANS = 'max,min') }}

## Exercice 2
Écrire une fontion `g` qui prend en paramètre un nombre $x$ et qui renvoie l'image de $x$ par la fonction $g ~:~  x \mapsto \dfrac{2-x}{3}$.

{{ IDE('../Exercices/exo_g/g', MAX = 5, SANS = 'max,min') }}