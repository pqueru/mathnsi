# Logiciels
[Thonny](https://thonny.org/){target=_blank} - Python IDE avec debuggeur  
[Anaconda](https://www.anaconda.com/products/individual){target=_blank} - Anaconda, utile en particulier pour JupyterLab et Jupyter Notebook  
[VS Codium](https://vscodium.com/){target=_blank} - multiplateformes et très complet  
[Geany](https://www.geany.org/){target=_blank} - multiplateformes supporte Pytho, HTML, PHP, Java, C, LaTeX  

[SQLite](https://sqlitebrowser.org/dl/){target=_blank} - DB Browser for SQLite  

## Serveurs Web
[MAMP](https://www.mamp.info/en/downloads/){target=_blank} - MacOs  
[WAMP](https://www.wampserver.com/en/){target=_blank} - Windows  
[XAMPP](https://sourceforge.net/projects/xampp/){target=_blank} - Multiplateforme et plus complet  