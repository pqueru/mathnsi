# Mathématiques

#### [Veritasium](https://www.youtube.com/c/veritasium){target=_blank} - Vulgarisation et culture générale mathématique
??? info "en particulier ..."
    === "Invention des nombres complexes"
        <iframe width="950" height="501" src="https://www.youtube.com/embed/cUzklzVXJwo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### [La chaîne Youtube d'El Jj](https://www.youtube.com/channel/UCgkhWgBGRp0sdFy2MHDWfSg){target=_blank}

#### [Histoires des mathématiques](https://hist-math.fr/){target=_blank} - Site très complet