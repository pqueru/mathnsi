# Informatique

#### [Science étonnante - Le site de David Louapre](https://scienceetonnante.com/){target=_blank}

??? info "en particulier ..."
    === "P=NP"
        <iframe width="668" height="376" src="https://www.youtube.com/embed/AgtOCNCejQ8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    === "Wordle"
        <iframe width="917" height="516" src="https://www.youtube.com/embed/iw4_7ioHWF4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        Sur le même thème mais du site 3blue1brown :
        <iframe width="950" height="534" src="https://www.youtube.com/embed/v68zYyaEmEA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### [3 blue 1 brown](https://www.3blue1brown.com/){target=_blank} - Vulgarisation informatique (pas si facile)

#### [Le site de Nicky Case](https://ncase.me/){target=_blank} - [The evolution of trust](https://ncase.me/trust){target=_blank} 

#### [PEP](https://pep20.org/){target=_blank} - Bonnes pratiques en Python

#### [xkcd](https://xkcd.com/){target=_blank} - Des comics

### ⭐ Concours

#### [Castor](https://castor-informatique.fr/){target=_blank} - Concours castor

#### [Concours canadien d'informatique](https://www.cemc.uwaterloo.ca/contests/ccc-cco-f.html){target=_blank} - Concours d'informatique

#### [Prologin](https://prologin.org/){target=_blank} - Concours d'informatique

### 🚀 Défis

#### [Codingame](https://codingame.com/){target=_blank} - Des défis

#### [PyDéfis](https://pydefis.callicode.fr/){target=_blank} - Défis informatiques

### Manuels
#### [Terminale NSI](https://dav74.github.io/site_nsi_term/){target=_blank}
#### [Première NSI](https://dav74.github.io/site_nsi_prem/){target=_blank}

### Tris
??? info "Une petite danse ?"
    === "Tri fusion"
        <iframe width="668" height="376" src="https://www.youtube.com/embed/XaqR3G_NVoo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    === "Tri par sélection"
        <iframe width="668" height="376" src="https://www.youtube.com/embed/Ns4TPTC8whw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    === "Tri par insertion"
        <iframe width="668" height="376" src="https://www.youtube.com/embed/ROalU379l3U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>